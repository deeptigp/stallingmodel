# Fix appending list to list of lists that should help me in generating patterns for all the videos created

#!/usr/bin/env python
import getVideoDuration
import random
import numpy
# Initialize the values of the percentage of the two categories of test cases we are considering : 1. Only Initial 2. Initial + Rebuf
classDist = [0.05,0.8146]
totalDuration = 7200
initialBufDistr = [ 0.5,0,0.5]
initialBufRange = { 0 :[0,7] , 1:[5,9],2:[8,20]}
initialBudget = classDist[0]*totalDuration
combinedBudget = classDist[1]*totalDuration


SFMBudget = int(combinedBudget*0.5*0.5*0.5)
SFLBudget = int(combinedBudget*0.5*0.5*0.5)

SMSBudget = int(combinedBudget*0.5*0.5*0.5)
SMMBudget = int(combinedBudget*0.5*0.5*0.5)

LFMBudget = int(combinedBudget*0.5*0.5*0.5)
LFLBudget = int(combinedBudget*0.5*0.5*0.5)

LMMBudget = int(combinedBudget*0.5*0.5*0.5)
LMSBudget = int(combinedBudget*0.5*0.5*0.5)

print('sfm budget',SFMBudget)
print('sfl budget',SFLBudget)
print('sms budget',SMSBudget)
print('smm budget',SMMBudget)

print('lfm budget',LFMBudget)
print('lfl budget',LFLBudget)

print('lmm budget',LMMBudget)
print('lms budget',LMSBudget)



#Number of events to sample
rebufEventType = ['few','many']
rebufEventTypeDist = [0.5,0.5]
rebufEvents  = {'few':[1,3],'many':[3,7]}

# Duration range cateogories 
rebufDuration = {'short':[2,4],'medium':[5,9],'long':[10,15]}


#posDistr = {'beginning':[0,1/4],'middle':[1/4,3/4],'end':[3/4,1] }
posDistr = [[0,1.0/2],[1.0/4,3.0/4],[1.0/2,1]]

#1. Read videos from videonames.txt into a list.

with open('videoSet2.txt', 'r') as f:
    input_videos = [line.strip() for line in f]

###


totalVideos = len(input_videos)
#Capture the total video lengths
video_lengths = [getVideoDuration.main(v) for v in input_videos]
pick_counter = [0]*totalVideos
pickThreshold = 8
numVideosInClasses = [int(round(i*totalVideos)) for i in classDist]
## Shuffle the videos , Videos can now be accessed as input_videos[shuffleIndexes[i]]
shuffleIndexes = range(0,totalVideos)


def pickVideosPerBudget(budget,rebufLen,fewOrMany):
	random.shuffle(shuffleIndexes)
	retList =[]
	currBudget =0
	for i in shuffleIndexes:
		if(fewOrMany == 'many'):
			if('Apple-iPhone4STVAd-Siri.mp4' in input_videos[i] or 'VerizoniPhone4SAd.mp4' in input_videos[i]):
				continue
		#length of this video + currBudget < budget, pick it. increase its pick count by 1.
		if(video_lengths[i] + rebufLen + currBudget <= budget):
			if(pick_counter[i] < pickThreshold):
				retList.append(i)
				pick_counter[i]+=1
				currBudget+=video_lengths[i]+rebufLen
	return retList
		
def genIndexArray(I1,I2):
	retL = []
	for (a,b) in zip(I1,I2):
		c=[a]*b
		retL.extend(c)
	return retL

def genIndexDist(cnt,I1):
	retL=[]
	retL = [int(round(i * cnt)) for i in I1]
	if(sum(retL) > cnt):
		retL[len(retL)-1]-=(sum(retL)-cnt)
	if(sum(retL) < cnt):
		retL[len(retL)-1]+=(cnt-sum(retL))
	return retL

### Writing the given pattern into the given filename
def write_file(vidIndexes,filePostfix,patterns,durPattern):
	for i in range(0,len(patterns)):
		if(len(durPattern)==0):
			fName=input_videos[vidIndexes[i]]+filePostfix
		else:
			fName=input_videos[vidIndexes[i]]+'_' +str(durPattern[i])+filePostfix
		fp = open(fName,'w')
		if (isinstance(patterns[i][0],list)):
			for p in patterns[i]:
				p1=map(str,p)
				fp.write(p1[0]+ '  ' + p1[1]+'\n')
		else:
			fp.write(str(patterns[i][0]) + ' ' + str(patterns[i][1]) + "\n")
		
		fp.write(str(1000) + ' ' + str(20) +"\n")
		fp.close()

###

### Adding only initial rebuffering pattern
## Generates all the initial rebuffering events (both short and long)
def only_initial_main():
		sRebufDur =  (initialBufRange[0][1]+initialBufRange[0][0])/2.0
		print(sRebufDur)
		shortInit = pickVideosPerBudget(initialBudget*initialBufDistr[0],sRebufDur,'none')
		print('lenth of short init', len(shortInit))
		#Generate short initial tests
		patterns = initialRebufPatternGenerator(0,len(shortInit))
		#for i in range(0,len(patterns)):
		write_file(shortInit,'_shortInitial.txt',patterns,[])


		lRebufDur =  (initialBufRange[2][1]+initialBufRange[2][0])/2.0
		print(lRebufDur)
		longInit = pickVideosPerBudget(initialBudget*initialBufDistr[2],lRebufDur,'none')
		print('lenth of long init', len(longInit))
		patterns =[]	
		#Generate long initial tests
		patterns = initialRebufPatternGenerator(2,len(longInit))
		#for i in range(0,len(patterns)):
		write_file(longInit,'_longInitial.txt',patterns,[])

def combinedIR(IPatterns,RPatterns):
	patterns=[]
	for a,b in zip(IPatterns,RPatterns):
		b.insert(0,a)
		patterns = patterns +[b]

	return patterns

def makeDurationPatterns(start, end, count, separation):
	patterns = []
	numLoops=0
	while len(patterns) < count:
		numLoops+=1
		val = random.randint(start, end)
		if len([i for i in patterns if i+separation > val and i-separation < val]) == 0:
			patterns.append(val)
		elif numLoops > 500:
			print("Too many loops: {0}, {1}, {2}, {3}, {4}".format(start, end, count, separation, patterns))
			return None
	return patterns

# number of videos, few/many , medium/short/long
def onlyRebufPatternGenerator(durPattern,videoIndexes,frequency,lenType):
	SEPARATE_S = 3
	rbufCount = len(durPattern)
	allPatterns = []
	print(frequency,lenType,durPattern)
	for i in range(0,rbufCount):
		#Get the number of events

		numE=random.sample(range(*rebufEvents[frequency]),1)
		
		#print('num', numE)
		numE=numE[0]
		
		# Get the durations for each event
		# durE=random.sample(range(*rebufDuration[lenType]),numE)

		durE = [random.randint(*rebufDuration[lenType]) for k in range(numE)]
		vLen = video_lengths[videoIndexes[i]]

		print(input_videos[videoIndexes[i]])

		#need to randomly generate locations
		if durPattern[i] == 3:
			randPos = makeDurationPatterns(SEPARATE_S, vLen-SEPARATE_S, numE, SEPARATE_S)
		else:
			rnge= posDistr[durPattern[i]]
			print('durPattern', durPattern[i])
			print('range',rnge)
			randPos = makeDurationPatterns(int(rnge[0]*vLen+SEPARATE_S), int(rnge[1]*vLen), numE, SEPARATE_S)

		randPos.sort()
		print(randPos,"===")
		#generate pattern			
		patt= [ [i,j] for (i,j) in zip(randPos,durE)]
		allPatterns.append(patt)
	
	return allPatterns
		
	#Number of events to sample
	#rebufEventType = ['few','many']
	#rebufEventTypeDist = [0.5,0.5]
	#rebufEvents  = {'few':[1,2],'many':[4,5]}

	# Duration range cateogories 
	#durType = ['short','medium','long']
	#rebufDuration = {'short':[3,4],'medium':[7,8],'long':[10,12]}
	#rebufDistr= {'few':[0.3,0.3,0.3],'many':[0.7,0.3,0]}


def initialRebufPatternGenerator(category,vidCount):
	patternList=[];
	for i in range(0,vidCount): 
		value=initialBufRange[category]
		if len(value) == 1:
			bufLen = value[0]
		
		if len(value) == 2:
			bufLen = random.randint(value[0]+1,value[1])
		patt = [0, bufLen]
		patternList.append(patt)	
	
	return patternList

def combinedPatternGenerator(initialType,vidIndexList,durDist,freq,lenType,fPostFix):
	#The four length array represets B,M,E,R (begin,middle,end,random)
	posDist = genIndexDist(len(vidIndexList),durDist)
	patternAssign = genIndexArray(range(0,len(durDist)),posDist)

	#print("Prints the distribution of the patterns into the three types")	
	print(posDist)	
	# We now have few,medium and positions for each video --> generate this pattern
	IPatterns = initialRebufPatternGenerator(initialType,len(vidIndexList))
	RPatterns = onlyRebufPatternGenerator(patternAssign,vidIndexList,freq,lenType)
	
	combi = combinedIR (IPatterns,RPatterns)
		
	write_file(vidIndexList,fPostFix,combi,patternAssign)

def combined_patterns_main():
	#Case1.1.1 Short init + few + medium/long 
	shortInit = (initialBufRange[0][1]+initialBufRange[0][0])/2.0 
	longInit = (initialBufRange[1][1]+initialBufRange[1][0])/2.0 

	few = (rebufEvents['few'][1]+rebufEvents['few'][0])/2 
	many = (rebufEvents['many'][1]+rebufEvents['many'][0])/2 
	
	
	short = (rebufDuration['short'][1]+rebufDuration['short'][0])/2;
	medium = (rebufDuration['medium'][1]+rebufDuration['medium'][0])/2;
	lng = (rebufDuration['long'][1]+rebufDuration['long'][0])/2;
	
	# avg. duration of the rebuf event
	#D.1.1.1.*
	sfm = int(shortInit + few* medium)
	SFMList =pickVideosPerBudget(SFMBudget,sfm,'few') 
	print('SFMlist',len(SFMList))
	combinedPatternGenerator(0,SFMList,[0.33,0.33,0.34],'few','medium','_sfm.txt')
	
	#avg. duration of the rebuf event
	sfl = int(shortInit + few*lng)
	SFLList =pickVideosPerBudget(SFLBudget,sfl,'few') 
	print('sfllist',len(SFLList))
	combinedPatternGenerator(0,SFLList,[0.33,0.33,0.34],'few','long','_sfl.txt')



	#Case1.1.2 Short init +many + short/medium
	sms = int(shortInit + many*short)
	smm = int(shortInit + many*medium)

	SMSList =pickVideosPerBudget(SMSBudget,sms,'many') 
	print('smslist',len(SMSList))
	combinedPatternGenerator(0,SMSList,[0.33,0,0.33,0.34],'many','short','_sms.txt')

	
	SMMList =pickVideosPerBudget(SMMBudget,smm,'many') 
	print('smmlist',len(SMMList))
	combinedPatternGenerator(0,SMMList,[0.33,0,0.33,0.34],'many','medium','_smm.txt')
	
	#Case3.1.1
	
	lfm = int(longInit + few*medium)
	lfl = int(longInit + few*lng)
	
	LFMList = pickVideosPerBudget(LFMBudget,lfm,'few')
	print('lfmlist',len(LFMList))
	combinedPatternGenerator(2,LFMList,[0.33,0.33,0.34],'few','medium','_lfm.txt')
	
	LFLList = pickVideosPerBudget(LFLBudget,lfl,'few')
	print('lfllist',len(LFLList))
	combinedPatternGenerator(2,LFLList,[0.33,0.33,0.34],'few','long','_lfl.txt')

	lmm = int(longInit + many*medium)
	lms = int(longInit + many*short)
	
	LMMList = pickVideosPerBudget(LMMBudget,lmm,'many')
	print('lmmlist',len(LMMList))
	combinedPatternGenerator(2,LMMList,[0.33,0,0.33,0.34],'many','medium','_lmm.txt')

	LMSList = pickVideosPerBudget(LMSBudget,lms,'many')
	print('lmslist',len(LMSList))
	combinedPatternGenerator(2,LMSList,[0.33,0,0.33,0.34],'many','short','_lms.txt')


def allPatternsGenerator():
	only_initial_main()
	combined_patterns_main()

if __name__ == "__main__":
    allPatternsGenerator()

