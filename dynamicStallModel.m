clear all
close all
load('vidStallData_noTrain.mat');

MOS = 5-DMOS;

eps = (CIhigh - CIlow)/2;

groupInds = find(videoGroups~=-1); %videos in the short stall group
groupStallWaveforms = stallWaveforms(groupInds);
groupNumDelays = numDelays(groupInds);
groupTimeSincePrev = timeSincePrev(groupInds);
groupMOS = MOS(groupInds);
groupEps = eps(groupInds);

inputNum = 1;

%High and Low confidence intervals.
groupCIhigh = groupMOS+groupEps;
groupCIlow = groupMOS-groupEps;

r = 3; % Window size (Model order)
nf = r;  %% The number of coefficients of f (11) in the paper.
nb = nf; %% The number of coefficients of b (11) in the paper.

numVideos = numel(groupInds);

%%Model parameters : Initialization.
% w: beta in the paper, represents the input non-linearity parameters.
% (eqn. 12)
% B and F for the Hammerstein model.
% V are the coeffficients of the linear Y (eqn (13), except we are not
% making it linear).


w = [0.115 2 -1 6]'; %[0.115 2.3 -150 180]';
b = 0.01*ones(nb+1,1); %0.03*ones(nb+1,1);
f = 0.01*ones(nf,1); %0.008*ones(nf,1);
v = [0.5 2.5]';
shrink = 1;
beta = 0.7;
alpha = 0.1;

itr = 0;
preitr = 0;
cost = 1000; %
newcost = 10000; %
nu = 0.8;

debug = 0;

%% All model parameters are concatenated.. computing its length..
dimtheta = length(w) + length(b) + length(f) + length(v);

%% Keeps the length of each parameter, to enable extracting separate parameters. In the same order as is being concatenated below.
modelParamsLength = [length(v) length(w)  length(b)  length(f)];

epsilon = shrink*(groupCIhigh-groupCIlow); %% Computing for all the videos..

while nu < 200 % Param Optimization.

    cost = 1000;
    newcost = 10;
    
    %% Gradient descent search Algo#2 in Chao's paper.
    while cost-newcost > 1e-5
        
        cost = 0;
        itr = itr + 1;
        
        gradient = zeros(dimtheta,1);
       
        omega = 1;
        theta = cat(1,v,w,b,f);
        
        tempf = 5*ones(length(f),1);
        
        
        % Fetch the outage rate first.
        switch inputNum
            case 1
                [Unu,GUnu] = computeOutageRate(groupStallWaveforms,epsilon,groupMOS,theta,modelParamsLength,r,nu,0);
            case 2
                [Unu,GUnu] = computeOutageRate(groupNumDelays,epsilon,groupMOS,theta,modelParamsLength,r,nu,0);
            case 3
                [Unu,GUnu] = computeOutageRate(groupTimeSincePrev,epsilon,groupMOS,theta,modelParamsLength,r,nu,0);
        end
        
        Eapx_nu = mean(Unu); %E^apx_nu; eq(16)
        cost = Eapx_nu;
      %  disp('size of GUnu');
      %  size(GUnu)
    
        
        %% For every video. 
        for seq = 1:numVideos
            
            %% Initializations.
            %1. Input initialization.
            in1 = groupStallWaveforms{1,seq};
            in2 = groupNumDelays{1,seq};
            in3 = groupTimeSincePrev{1,seq};
            T = numel(in1); %length of current video in seconds
            
            %2. MOS of this specific video seq.
            currMOS = groupMOS(seq);  
            switch inputNum
                case 1
                    [temp_Gv,temp_Gw,temp_Gb,temp_Gf] = gradientDescent(in1,theta,modelParamsLength,currMOS,r);
                case 2
                    [temp_Gv,temp_Gw,temp_Gb,temp_Gf] = gradientDescent(in2,theta,modelParamsLength,currMOS,r);
                case 3
                    [temp_Gv,temp_Gw,temp_Gb,temp_Gf] = gradientDescent(in3,theta,modelParamsLength,currMOS,r);
            end
           
            %% update gradient..
            gradient = gradient + 2*cat(1,temp_Gv,temp_Gw,temp_Gb,temp_Gf)*(GUnu(seq)*ones(T,1)); 
          %  size(gradient)
            if(debug == 1)
               gradient  = randn(dimtheta,1)'.*randperm(400,dimtheta);
               gradient = gradient';  
            end
        end %% The for loop of each video.
        
        gradient = gradient/numVideos; %% Value in line 2 of Algo#2
        

        newcost = mean(Unu);
        while ((newcost > cost - alpha*omega*gradient'*gradient) || (max(abs(roots(cat(1,1,-flipud(tempf))))) >= 1)) %%  Line 3 of Algo. 2
            if omega < 1e-4
                break;
            end
            omega = beta * omega;
            temptheta = theta - omega*gradient;
            tempf = temptheta(length(v)+length(w)+length(b)+1:length(theta));
            
            switch inputNum
                case 1
                    [Unu,GUnu] = computeOutageRate(groupStallWaveforms,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,0);
                case 2
                    [Unu,GUnu] = computeOutageRate(groupNumDelays,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,0);
                case 3
                    [Unu,GUnu] = computeOutageRate(groupTimeSincePrev,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,0);
            end
            
            newcost = mean(Unu);
        %    disp('inside the second while')
           % omega
            %temptheta
        end
        
        optlog(itr) = newcost;
        
        %Count the number of badly-predicted final DMOS
        switch inputNum
            case 1
                [Unu,GUnu,predictedScores] = computeOutageRate(groupStallWaveforms,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,0);
            case 2
                [Unu,GUnu,predictedScores] = computeOutageRate(groupNumDelays,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,0);
            case 3
                [Unu,GUnu,predictedScores] = computeOutageRate(groupTimeSincePrev,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,0);
        end
        
        numerror = 0; 
        
        for seq = 1:numVideos
            if predictedScores(seq) > groupMOS(seq) + epsilon(seq) || predictedScores(seq) < groupMOS(seq) - epsilon(seq)
                numerror = numerror + 1;
            end
        end
        theta = temptheta;
        
        [v, w, b, f] = extractIndividualModelParams(theta,modelParamsLength);
        %% Iteration check.
        if itr-preitr >= 500
            break;
        end  
       
    end
    [nu cost newcost]
     theta
     
    nu = nu * 1.2; %Algorithm 1, line 4
    preitr = itr; %previous # iterations
end

%% Get the videos that fall outside the conf. interval
numerror = 0;
switch inputNum
    case 1
        [Unu,GUnu,predictedScores] = computeOutageRate(groupStallWaveforms,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,1);
    case 2
        [Unu,GUnu,predictedScores] = computeOutageRate(groupNumDelays,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,1);
    case 3
        [Unu,GUnu,predictedScores] = computeOutageRate(groupTimeSincePrev,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,1);
end

for seq = 1:numVideos
    if predictedScores(seq) > groupMOS(seq) + epsilon(seq) || predictedScores(seq) < groupMOS(seq) - epsilon(seq)
        numerror = numerror + 1;
    end
    [predictedScores(seq) groupMOS(seq)]
end
numerror %The last computed numerror can be printed..

globalOutageRate = mean(Unu)