%get avvasiMOS
clear all

load('vidStallData_noTrain.mat','names','DMOS');

%% Group A
fid = fopen('MOS-a.csv');
out = textscan(fid, '%s%f','delimiter',',');
fclose(fid);

MOSnamesA = out{1,1};
MOSvaluesA = out{1,2};

refVidIndsA = [];
for i = 1:numel(MOSnamesA)
    currName = MOSnamesA{i,1};
    if isempty(strfind(currName,'_sfm')) && ...
            isempty(strfind(currName,'_lfm')) && ...
            isempty(strfind(currName,'_lmm')) && ...
            isempty(strfind(currName,'_sms')) && ...
            isempty(strfind(currName,'_lms')) && ...
            isempty(strfind(currName,'_lfl')) && ...
            isempty(strfind(currName,'_sfl')) && ...
            isempty(strfind(currName,'_smm')) && ...
            isempty(strfind(currName,'_shortInitial')) && ...
            isempty(strfind(currName,'_longInitial'))
        refVidIndsA = [refVidIndsA;i];
    end
end

refVidNamesA = MOSnamesA(refVidIndsA,1);
%remove '.mp4' from each reference video name
for i = 1:numel(refVidNamesA)
    currName = refVidNamesA{i,1};
    refVidNamesA{i,1} = currName(1:end-4);
end

names_refVidInd = zeros(1,numel(names));
for i = 1:88 %numel(names)
    currName = names{1,i};
    refInd = 0;
    for j = 1:numel(refVidNamesA)
        if ~isempty(strfind(currName,refVidNamesA{j,1}))
            refInd = j;
        end
    end
    names_refVidInd(1,i) = refInd;
end

%% Group B
fid = fopen('MOS-b.csv');
out = textscan(fid, '%s%f','delimiter',',');
fclose(fid);

MOSnamesB = out{1,1};
MOSvaluesB = out{1,2};

refVidIndsB = [];
for i = 1:numel(MOSnamesB)
    currName = MOSnamesB{i,1};
    if isempty(strfind(currName,'_sfm')) && ...
            isempty(strfind(currName,'_lfm')) && ...
            isempty(strfind(currName,'_lmm')) && ...
            isempty(strfind(currName,'_sms')) && ...
            isempty(strfind(currName,'_lms')) && ...
            isempty(strfind(currName,'_lfl')) && ...
            isempty(strfind(currName,'_sfl')) && ...
            isempty(strfind(currName,'_smm')) && ...
            isempty(strfind(currName,'_shortInitial')) && ...
            isempty(strfind(currName,'_longInitial'))
        refVidIndsB = [refVidIndsB;i];
    end
end

refVidNamesB = MOSnamesB(refVidIndsB,1);
%remove '.mp4' from each reference video name
for i = 1:numel(refVidNamesB)
    currName = refVidNamesB{i,1};
    refVidNamesB{i,1} = currName(1:end-4);
end

% names_refVid = zeros(1,numel(names));
for i = 89:176 %numel(names)
    currName = names{1,i};
    refInd = 0;
    for j = 1:numel(refVidNamesB)
        if ~isempty(strfind(currName,refVidNamesB{j,1}))
            refInd = j;
        end
    end
    names_refVidInd(1,i) = refInd;
end

%% Get reference scores and MOS scores
refScores = zeros(1,176);
mosScores = zeros(1,176);
for i = 1:176
    currName = names{1,i};
    if i < 89
        refVidInd = refVidIndsA(names_refVidInd(i));
        refScores(1,i) = MOSvaluesA(refVidInd,1);
        for j = 1:numel(MOSnamesA)
            if isequal(MOSnamesA{j,1},currName)
                mosScores(1,i) = MOSvaluesA(j,1);
            end
        end
            
    else
        refVidInd = refVidIndsB(names_refVidInd(i));
        refScores(1,i) = MOSvaluesB(refVidInd,1);
        for j = 1:numel(MOSnamesB)
            if isequal(MOSnamesB{j,1},currName)
                mosScores(1,i) = MOSvaluesB(j,1);
            end
        end
    end
end

%% Compute Avvasi DQS: DQS = (MOS) + (5 - Ref)

dqsScores = mosScores + (5 - refScores);

save('DQS_MOS_Ref_scores.mat','dqsScores','mosScores','refScores')