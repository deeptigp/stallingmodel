function sol = hobfieldQoE(inputData)

numTests = numel(inputData);
% [~,numIn] = size(inputData{1,1}); %should have 3: stall waveforms, time since previous delay, and number of delays

sol = zeros(1,numTests);

for i = 1:numTests
    currTest = inputData{1,i};
    stallWaveform = currTest(:,1);
    L = sum(stallWaveform~=0);
    numDelays = currTest(:,3); % # of delays is 3rd column
    N = numDelays(end);
    sol(1,i) = 3.5 * exp(-(0.15*L + 0.19)*N) + 1.5;
end