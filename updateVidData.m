%vidStallDataParameters
load('vidStallData.mat')
%% To remove training files from every element of the mat file.
set = [89 90 179 180];

CIlow(set)=[]; 
CIhigh(set)=[];
DMOS(set)=[];
fps(set) = [];
videoGroups(set)=[];
vidLengths_s(set) = [];
names(set) = '';
numDelays(set) = '';
stallLengths_s(set) = '';
stallLocs_s(set) = '';
stallWaveforms(set) = '';
timeSincePrev(set) = '';

save('vidStallData_noTrain.mat','CIlow','CIhigh','DMOS','fps','videoGroups','vidLengths_s','names','numDelays','stallLengths_s','stallLocs_s','stallWaveforms','timeSincePrev');