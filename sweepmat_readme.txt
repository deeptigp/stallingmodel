sweep.mat contains:
- models: 1x176 cell
- perf: 1x176 cell
for the 176 videos

if video v is not a reference video,
- models{1,v} contains a 10x10 cell, where the i,j'th location
	contains a nonlinear hammerstein wiener model for i=nf, j=nb
	(nf, nb are number of coefficients that describe linear filter)
- perf{1,v} contains a 10x10 double, where the i,j'th element is the
	final prediction error with the i,j'th model

sweepSigmoid.mat:
for each nf and nb in {1,...,5}, create a model for
different # of units for the input and output sigmoid functions using:
numberOfUnits in {5,10,15,20,25,30,35}
-> so, for each pair (nf,nb) for each video, perf has a measure of
performance for 6 different numberOfUnits
-> perf is 1x176 cell; each cell (if the video is not a reference video)
has a 5x5 cell, each of which contains a 6x1 vector corresponding to the 6
numberOfUnits tested