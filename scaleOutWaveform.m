function newOut = scaleOutWaveform(outWaveform,gTruthMOS)
% gTruthMOS = (gTruthMOS + outWaveform(end)) / 2;
if outWaveform(end)==5
    newOut = outWaveform;
else
    d1 = [0; outWaveform(1:end-1)-outWaveform(2:end)];
    d2 = outWaveform - outWaveform(end);
    newOut = 5*ones(numel(outWaveform),1);
    for i = 2:numel(outWaveform)
        if d2(i-1)==0
            newOut(i) = newOut(i-1);
        else
            newOut(i) = newOut(i-1)-(newOut(i-1)-gTruthMOS)*d1(i)/d2(i-1);
        end
    end
end

% currMOS = outWaveform(end);
% d = currMOS - gTruthMOS;
% newOut = outWaveform - d;