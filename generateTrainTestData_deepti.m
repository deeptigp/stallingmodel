function [trainData, testData,avvasiTestMOS, gTruthTestMOS] = generateTrainTestData(splitNum)
%% Which train/test split to choose. 
    %splitNum = 1;
    load('vidStallData_noTrain.mat');
    load('outWaveforms.mat');
    
    %%Training Data.
    load('TrainingNIter80-20allGroups.mat');
    load('TestingNIter80-20allGroups.mat');
    
    MOS = 5-DMOS;
    
    avvasiMOS =[];
    
    for i = 1:length(outWaveforms)
        o = outWaveforms{1,i};
        avvasiMOS = [avvasiMOS  o(end)];
%         outWaveforms{1,i} = scaleOutWaveform(o,MOS(i));
    end
    
    TrainingNIter = TrainingNIter(:,splitNum);
    TestingNIter = TestingNIter(:,splitNum);
  %  ValidNIter = ValidNIter(:,splitNum);
    
    numTrainData = sum(TrainingNIter);
    numTestData = sum(TestingNIter);
 %   numValidData = sum(ValidNIter);
    
    
    trainDataX = cell(1,numTrainData);
    trainDataY = cell(1,numTrainData);
    
    testDataX  = cell(1,numTestData);
    testDataY  = cell(1,numTestData);
    
    %validDataX  = cell(1,numValidData);
    %validDataY  = cell(1,numValidData);
    
    trainIndxs = find(TrainingNIter);
    testIndxs = find(TestingNIter);
   % validIndxs = find(ValidNIter);
    
    trainMOS = MOS(trainIndxs);
    gTruthTestMOS = MOS(testIndxs);
   % validMOS = MOS(validIndxs);
    
    avvasiTestMOS = avvasiMOS(testIndxs);
    
    %%Training Data
    for i = 1:numTrainData
        indx = trainIndxs(i);
        
        in1 = stallWaveforms{1,indx};
        in2 = timeSincePrev{1,indx};
        in3 = numDelays{1,indx};
        
        if ~isempty(find(isnan(in1))) || ~isempty(find(isnan(in2))) || ~isempty(find(isnan(in3)))
            a = 'debug';
        end
        
        trainDataX{i} = [in1, in2, in3];
        trainDataY{i} = outWaveforms{1,indx};     
    end
    
    %% Valid data.
%     for i = 1:numValidData
%         indx = validIndxs(i);
%         
%         in1 = stallWaveforms{1,indx};
%         in2 = timeSincePrev{1,indx};
%         in3 = numDelays{1,indx};
%         
%         validDataX{i} = [in1, in2, in3];
%         validDataY{i} = outWaveforms{1,indx};     
%     end
    
    for i = 1:numTestData
        indx = testIndxs(i);
        
        in1 = stallWaveforms{1,indx};
        in2 = timeSincePrev{1,indx};
        in3 = numDelays{1,indx};

        out = outWaveforms{1,indx};

        testDataX{i} = [in1, in2, in3];
%         testDataY{i} = ones(length(outWaveforms{1,indx}),1)*5;
        testDataY{i} = outWaveforms{1,indx};
    end
    
    trainData = iddata(trainDataY,trainDataX,1);
    %validData = iddata(validDataY,validDataX,1);
    testData = iddata(testDataY,testDataX,1);
%end