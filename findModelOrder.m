%
clear all
load('vidStallData.mat');

numVids = numel(names);

rmax = 20;
rmin = 2;
numrs = rmax - rmin + 1; %number of r's
n = 2; %parameter for cvx; # t's
Qrs = cell(1,numVids);
Qrs_avg = zeros(1,numVids);
isConst = ones(1,numVids);

for i = 1:numVids
    num = DMOS(i); %numerator (eq21)
    T = vidLengths_s(i); %length of video
    Qr = zeros(1,numrs);
    q = stallWaveforms{i};
    counter = 1;
    for r = rmin:rmax
        currMax = 0;
        for t1 = 1:T-1
            for t2 = t1+1:T
                den = norm(phi_rt(q,r,t1)-phi_rt(q,r,t2)); %denominator (eq21)
                tmp = num/den;
                if tmp > currMax && ~isnan(tmp) && ~isinf(tmp)
                    currMax = tmp; %keep track of current maximum
                end
            end
        end
        Qr(counter) = currMax;
        counter = counter + 1;
    end
    Qrs{i} = Qr;
    Qrs_avg(i) = mean(Qr);
    if Qr/max(Qr) ~= ones(1,numrs) & videoGroups(1,i) ~= -1
        isConst(i) = 0;
    end
end

indss = find(videoGroups == 1); %indices of videos with short stalls
indsm = find(videoGroups == 2); %indices of videos with medium stalls
indsl = find(videoGroups == 3); %indices of videos with long stalls

Qs_s = Qrs_avg(indss);
Qs_m = Qrs_avg(indsm);
Qs_l = Qrs_avg(indsl);

Qavg_s = mean(Qrs_avg(indss)); %find average Q value for short stalls
Qavg_m = mean(Qrs_avg(indsm)); %find average Q value for medium stalls
Qavg_l = mean(Qrs_avg(indsl)); %find average Q value for long stalls