function newWaveform = removeTrainingFromWaveform(waveform)
%remove training
% clear all
% load('sw_nd_exp05_exp05.mat');
newWaveform = cell(1,176);

j = 1;
for i = 1:180
    if i~=89 && i~=90 && i~=179 && i~=180
        newWaveform{1,j} = waveform{1,i};
        j = j + 1;
    end
end