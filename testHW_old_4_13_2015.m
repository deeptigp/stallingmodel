%Test Hammerstein-Wiener Model
clear all
clc

load('outWaveforms.mat');
load('vidStallData_noTrain.mat','stallWaveforms');
load('vidStallData_noTrain.mat','timeSincePrev');
load('vidStallData_noTrain.mat','numDelays');
load('vidStallData_noTrain.mat','videoGroups');

load('warn1.mat'); %load a warning to suppress it
warning('off',id);

numVids = numel(outWaveforms);
numIn = 3;
nk = zeros(1,numIn);
nfs = 10;
nbs = 10;
models = cell(1,numVids);
perf = cell(1,numVids);

for i = 1:numVids
    i %print out vid #
    if videoGroups(1,i)~=-1
        in1 = stallWaveforms{1,i};
        in2 = timeSincePrev{1,i};
        in3 = numDelays{1,i};
        inall = [in1, in2, in3];
%         out = [0;outWaveforms{1,i}]; %zero pad the output by 1
        out = outWaveforms{1,i}; %no need to zero pad for non-ref videos
        
        z1 = iddata(out,in1,1);
        z2 = iddata(out,in2,1);
        z3 = iddata(out,in3,1);
        zall = iddata(out,inall,1);
        
        InputNL = sigmoidnet('NumberOfUnits',20); %input nonlinearity
        InputNLs = [InputNL;InputNL;InputNL]; %same input nonlinearity for each of the 3 inputs
        OutputNL = sigmoidnet('NumberOfUnits',20); %output nonlinearity; piecewise linear better?
        
%         nf = 3*ones(1,numIn);
%         nb = nf-1;
        ms = cell(nfs,nbs);
        fpes = zeros(nfs,nbs);
        for j = 1:nfs
            nf = j*ones(1,numIn);
            for k = 1:nbs
                nb = k*ones(1,numIn);
                try
                    m = nlhw(zall,[nb,nf,nk],InputNLs,OutputNL);
                    ms{j,k} = m;
                    fpes(j,k) = m.EstimationInfo.FPE; %http://www.mathworks.com/help/ident/ref/fpe.html
                catch
                    ms{j,k} = -1;
                    fpes(j,k) = -1;
                end
            end
        end
        models{1,i} = ms;
        perf{1,i} = fpes;
    end
end

% save('orderSweep.mat','models','perf','InputNLs','OutputNL','nfs','nbs');
save('sweep.mat','models','perf');