% generate sawtooth waveform for each video
clear all
load('vidStallData.mat');

numVids = numel(DMOS);
stallWaveforms = cell(1,numVids);
numDelays = cell(1,numVids);
timeSincePrev = cell(1,numVids);
stallDensity = cell(1,numVids);
a = 0.5;
b = 0.5;
c = 0; %b/c imposing nonlinearity for timeSincePrev doesn't help

for i = 1:numVids
    currVidLength = vidLengths_s(i);
    currStalls = zeros(currVidLength+1,1); %initialize current waveform
    currDelays = zeros(currVidLength+1,1);
    currTime = 1111*ones(currVidLength+1,1);
    currDensity = zeros(currVidLength+1,1);
    currStallLocs_s = stallLocs_s{i}; %stall locations for current video
    currStallLengths_s = stallLengths_s{i}; %stall lengths for current video
    totalStallTime = 0;
    if currStallLocs_s == -1
        stallWaveforms{1,i} = currStalls;
        numDelays{1,i} = currDelays;
        timeSincePrev{1,i} = currTime;
        stallDensity{1,i} = currDensity;
    else
        numStalls = numel(currStallLocs_s)-1; %number of stalls in current video;
        %disregard stall locs of 1000 and stall lengths of 20s
        for j = 1:numStalls
            currStallLoc = currStallLocs_s(j) + totalStallTime;
            currStallLength = currStallLengths_s(j);
            totalStallTime = totalStallTime + currStallLength;
            stallLength = 0;
            for k = 1:currStallLength+1
                currStalls(currStallLoc+k) = exp(a*stallLength)-1;% exp(a*stallLength)-1; %a*log(1+stallLength);
                stallLength = stallLength+1;
                currTime(currStallLoc+k) = 0;
            end
            currDelays(currStallLoc+1:end) = currDelays(currStallLoc+1:end)+1;
            t1 = currStallLoc + currStallLength + 1;
            if j == numStalls
                t2 = currVidLength;
            else
                t2 = currStallLocs_s(j+1) + totalStallTime; %last time before next stall
            end
            t = 1;
            for k = t1:t2
                currTime(k+1) = t;
                t = t + 1;
            end

        end
        stallWaveforms{1,i} = currStalls;
        numDelays{1,i} = exp(b*currDelays)-1; %%currDelays; 
        if c == 0
            timeSincePrev{1,i} = currTime;
        else
            timeSincePrev{1,i} = exp(c*currTime)-1;
        end
        
        %%compute stallDensity
        vidLength = length(currDensity);
        for k = 1:vidLength
            currDensity(k) = k/currDelays(k);
            if(currDelays(k)==0)
                a = 'debug';
            end
        end
        stallDensity{1,i} = currDensity;
    end
    figure(1), plot(stallDensity{1,i})
end

% y1 = stallWaveforms{1,3};
% y2 = numDelays{1,3};
% y3 = timeSincePrev{1,3};

stallWaveforms = removeTrainingFromWaveform(stallWaveforms);
numDelays = removeTrainingFromWaveform(numDelays);
timeSincePrev = removeTrainingFromWaveform(timeSincePrev);
stallDensity = removeTrainingFromWaveform(stallDensity);

newFile = sprintf('newWaveforms_exp0%d_exp0%d_exp0%d.mat',a*100,b*100,c*100);
save(newFile,'stallWaveforms','numDelays','timeSincePrev');
save('abc.mat','a','b','c');

% %%save stallDensity
% newFile = 'stallDensity.mat';
% save(newFile,'stallDensity');

clear all
load('vidStallData_noTrain.mat');
load('abc.mat');
newFile = sprintf('newWaveforms_exp0%d_exp0%d_exp0%d.mat',a*100,b*100,c*100);
load(newFile);
newFile = sprintf('vidStallData_noTrain_exp0%d_exp0%d_exp0%d.mat',a*100,b*100,c*100);

% %%save stallDensity in vidStallData_noTrain
% load('stallDensity.mat');
% newFile = 'vidStallData_noTrain_stallDensity.mat';
clear a
clear b
clear c
save(newFile);

% figure(1);
% subplot(3,1,1), plot(y1);
% subplot(3,1,2), plot(y2);
% subplot(3,1,3), plot(y3);
