% load('sweep.mat'); %takes long time to load; load once at start of session
load('vidStallData_noTrain.mat','videoGroups');

perfResults = struct('bestnfs',cell(1,3),'bestnbs',cell(1,3),...
    'bestnf',cell(1,3),'bestnb',cell(1,3));

for i = 1:3
    groupInds = find(videoGroups==i);
    numGroupVids = numel(groupInds);
    recminnfs = cell(1,numGroupVids);
    recbestnfs = cell(1,numGroupVids);
    recminnbs = cell(1,numGroupVids);
    recbestnbs = cell(1,numGroupVids);
    recbestnf = zeros(1,numGroupVids);
    recbestnb = zeros(1,numGroupVids);
    for j = 1:numGroupVids
        currVid = groupInds(j);
        currPerf = perf{1,currVid};
        [minnfs,bestnfs] = min(abs(currPerf)); %bestnfs: nf (for each nb) for lowest error
        [minnbs,bestnbs] = min(abs(currPerf')); %bestnbs: nb (for each nf) for lowest error
        [minoverall,bestind] = min(abs(currPerf(:)));
        [bestnf,bestnb] = ind2sub(size(currPerf),bestind); %best nb and nf overall
        
        recminnfs{1,j} = minnfs;  %record best performance for best nf
        recbestnfs{1,j} = bestnfs; %record best nf
        recminnbs{1,j} = minnbs; %record best performance for best nb
        recbestnbs{1,j} = bestnbs; %record best nb
        recbestnf(1,j) = bestnf; %record best overall nf
        recbestnb(1,j) = bestnb; %record best overall nb
    end
    perfResults(i).bestnfs = recbestnfs;
    perfResults(i).bestnbs = recbestnbs;
    perfResults(i).bestnf = recbestnf;
    perfResults(i).bestnb = recbestnb;
    %for each group:
    mean(recbestnf)
    mode(recbestnf)
    mean(recbestnb)
    mode(recbestnb)
end