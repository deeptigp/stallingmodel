function sol = interruptDQS(t,Q0,T1,T2,a,m)
if t<T1
    dqs = Q0;
elseif t<T2
    dqs = Q0 - a/2 * (1 + cos(pi*(t-T2)/(T2-T1)));
else
    dqs = Q0 - (a + m*(t - T2));
end

if dqs < 1
    sol = 1;
else
    sol = dqs;
end