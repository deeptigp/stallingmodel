%Test Hammerstein-Wiener Model
clc
% load('outWaveforms.mat');
%load('warn2.mat'); %load a warning to suppress it
%warning('off',id);


nbs = [ 1 2 3 4 ];
numIn = 3;
nk = zeros(1,numIn);

allSigUnits = [ 5 10 15 20 25 ];
numSplits = 5;
% resultsByGroup = cell(1,3);

for groupNum = 1:3
    groupNum
    models = cell(1,numel(allSigUnits));
    
    corr_bySplit_avvasi_gTruth = zeros(1,numSplits);
    
    corr_bySigUnits_avvasi = zeros(numel(allSigUnits),1);
    corr_bySigUnits_gTruth = zeros(numel(allSigUnits),1);
    corr_byOrder_avvasi = zeros(numel(allSigUnits),numel(nbs));
    corr_byOrder_gTruth = zeros(numel(allSigUnits),numel(nbs));
    corr_bySplit_avvasi = cell(1,numSplits);
    corr_bySplit_gTruth = cell(1,numSplits);
    
    % pred_bySigUnits = zeros(numel(allSigUnits),numel(testMOS));
    pred_byOrder = cell(1,numel(nbs));
    pred_bySplit = cell(1,numSplits);
    for splitNum = 1:numSplits
        splitNum
        [trainData, testData, avvasiTestMOS, gTruthTestMOS] = generateTrainTestDataForGroup(splitNum,groupNum);
%         [trainData, testData, trainMOS, avvasiTestMOS] = generateTrainTestData(splitNum);

        testDataIn = iddata([],testData.InputData,1); %only input of testData
        
        for nbIdx = 1:numel(nbs)
            nb = nbs(nbIdx)*ones(1,numIn);
            nf = nb + 1;
            compareMat = cell(numel(allSigUnits,numel(avvasiTestMOS))); %to compare models
            pred_bySigUnits = zeros(numel(allSigUnits),numel(avvasiTestMOS));
            
            for unitsIdx = 1:numel(allSigUnits)
                numSigUnits = allSigUnits(unitsIdx);
                InputNL = sigmoidnet('NumberOfUnits',numSigUnits); %input nonlinearity
                InputNLs = [InputNL;InputNL;InputNL]; %same input nonlinearity for each of the 3 inputs
                OutputNL = sigmoidnet('NumberOfUnits',numSigUnits); %output nonlinearity
                try
                    m = nlhw(trainData,[nb,nf,nk],InputNLs,OutputNL);
                    models{1,unitsIdx} = m;
                    p = sim(m,testDataIn);
                    finalMOS = zeros(1,numel(avvasiTestMOS));
                    for testIdx = 1:numel(avvasiTestMOS)
                        currTest = getexp(p,testIdx);
                        finalMOS(1,testIdx) = currTest.OutputData(end);
                    end
                   corr_bySigUnits_avvasi(unitsIdx,1) = corr(finalMOS',avvasiTestMOS);
                   corr_bySigUnits_gTruth(unitsIdx,1) = corr(finalMOS',gTruthTestMOS);
                   pred_bySigUnits(unitsIdx,:) = finalMOS;
                catch
                    models{1,unitsIdx} = -1;
                    corr_bySigUnits_avvasi(unitsIdx,1) = -1;
                    corr_bySigUnits_gTruth(unitsIdx,1) = -1;
                    pred_bySigUnits(unitsIdx,:) = -1*ones(1,numel(avvasiTestMOS));
                end
                %             m = nlhw(trainData,[nb,nf,nk],InputNLs,OutputNL);
                %             models{1,unitsIdx} = m;
                %             p = sim(m,testDataIn);
                %             finalMOS = zeros(1,numel(testMOS));
                %             for testIdx = 1:numel(testMOS)
                %                 currTest = getexp(p,testIdx);
                %                 finalMOS(1,testIdx) = currTest.OutputData(end);
                %             end
                %             corr_bySigUnits(unitsIdx,1) = corr(finalMOS',testMOS');
                %             pred_bySigUnits(unitsIdx,:) = finalMOS;
            end
            corr_byOrder_avvasi(:,nbIdx) = corr_bySigUnits_avvasi;
            corr_byOrder_gTruth(:,nbIdx) = corr_bySigUnits_gTruth;
            pred_byOrder{1,nbIdx} = pred_bySigUnits;
        end
        %%== Compute the correlation between Avvasi's scores and ground
        %%truth MOS scores === %%
        corr_bySplit_avvasi_gTruth(1,splitNum) = corr(gTruthTestMOS,avvasiTestMOS);
        corr_bySplit_avvasi{1,splitNum} = corr_byOrder_avvasi;
        corr_bySplit_gTruth{1,splitNum} = corr_byOrder_gTruth;
        pred_bySplit{1,splitNum} = pred_byOrder;
    end
    resultsByGroup{1,groupNum} = {corr_bySplit_avvasi;corr_bySplit_gTruth;pred_bySplit};
end
