function [temp_Gv,temp_Gw,temp_Gb,temp_Gf] = gradientDescent(in1,theta,modelParamsLength,currMOS,r) 
    
    T = numel(in1);
    % vt refers to eqn (13)
    X = zeros(4,T);
    
    lenV = modelParamsLength(1);
    lenW = modelParamsLength(2);
    lenB = modelParamsLength(3);
    lenF = modelParamsLength(4);
    
    %%Extract model parameters to be used next..
    [v, w, b, f] = extractIndividualModelParams(theta,modelParamsLength);
    
    %% Temporary gradients. Initializations.
    temp_Gzv = zeros(lenV,T);
    temp_Gv = zeros(lenV,T);

    temp_Gzw = zeros(lenW,T);
    temp_Gw = zeros(lenW,T);

    temp_Gzb = zeros(lenB,T);
    temp_Gb = zeros(lenB,T);

    temp_Gzf = zeros(lenF,T);
    temp_Gf = zeros(lenF,T);

    %% Nonlinear input..
    ut = inputNonLinearity(in1,w); %eq(12)

    %%Gradient of U wrt beta ( appendix)
    X(1,:) = [sigmoid(ut,w(1),w(2),0,w(4)).*(1-sigmoid(ut,w(1),w(2),0,1)).*ut]';
    X(2,:) = [sigmoid(ut,w(1),w(2),0,w(4)).*(1-sigmoid(ut,w(1),w(2),0,1))]';
    X(3,:) = 1;
    X(4,:) = [sigmoid(ut,w(1),w(2),0,1)]';

    % Construct the output continous DMOS
    vt = constructLinFilterOutput(ut,currMOS,r,b,v,f);

    %% === Begin computation of gradients of each model parameter. ===

    for k = r+1:T % calculate gradient %in paper: gradient of gammas
        temp_Gzv(:,k) = temp_Gzv(:,k-r:k-1)*f(1:r);
        temp_Gv(:,k) = cat(1,vt(k,1),1) + temp_Gzv(:,k);
    end


    for k = r+1:T
        temp_Gzw(:,k) = X(:,k-r:k)*b+temp_Gzw(:,k-r:k-1)*f(1:r); %eq(28)
        temp_Gw(:,k) = v(1)*temp_Gzw(:,k); %eqn 26 but for linear output.
    end

    for k = r+1:T
        temp_Gzb(:,k) = ut(:,k-r:k)'+temp_Gzb(:,k-r:k-1)*f(1:r); %gradient of Z (paper : v) wrt b
        temp_Gb(:,k) = v(1)*temp_Gzb(:,k);
    end

    for k = r+1:T
        temp_Gzf(:,k) = vt(k-r:k-1) + temp_Gzf(:,k-r:k-1)*f; %gradient of Z (paper: v) wrt f
        temp_Gf(:,k) = v(1)*temp_Gzf(:,k);
    end
end