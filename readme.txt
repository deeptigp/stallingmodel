vidStallData.mat has the following entries:
a. videoGroups : -1: refVideo, 1 for short stalls, 2 for medium length stalls, 3 for long stalls.
b. stallLengths_s : the length of the stalls that appear at the location specified by stallLocs_s. Each entry ends with 20 ( as a way to indicate end of the stall pattern).

dynamicStallModel.m : The main driver file which refers to Algo1 in the paper.
gradientDescent.m : Algo2 from the paper.
constructLinFilterOutput.m : Takes in the non-linear input and constructs the v[t] in (11) and qhatt (13)
computeOutageRate : computes the outage rate and its derivative (16) and dU from (17)
extractIndividualModelParams: Given a concatenated theta, extracts individual parameters - w,v,b,f
inputNonLinearity: Given input in, computes u[t]  eqn. (12)