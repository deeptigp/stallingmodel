function sol = playbackDQS(t,Q0,T1,T2,a,m)
if t<T1
    dqs = Q0;
elseif t<T2
    dqs = Q0 + a/2 * (1 + cos(double(3.14159*(t-T2)/(T2-T1))));
else
    dqs = Q0 + (a + m*(t - T2));
end

if dqs > 5
    sol = 5;
else
    sol = dqs;
end