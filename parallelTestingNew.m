% parallelTesting but now also computing hobfield correlation

startTime  = cputime;

numSplits = 35;

nbs = 2;
% nf = nb-1;
allSigUnits = 29;

numIn = 3;
nk = zeros(1,numIn);

debug = 1;

p = gcp('nocreate');
if isempty(p)
    p = gcp;
end


numWorkers = max(1, p.NumWorkers) %% Typical value to expect == 4

if (numWorkers ==1)
    error('Parallel pooling not enabled, please check!');
end

numWorkers =1;

% == list of splits to assign per worker
workerSplitList = cell(1, numWorkers);

%% == Constructing the data prior to the parallel call.
modelData = cell(numSplits,1);

for i = 1:numSplits
    [trainData, testData, avvasiTestMOS1, gTruthTestMOS1] = generateTrainTestData(i);
    modelData{i} = { trainData; testData; avvasiTestMOS1; gTruthTestMOS1};
end

%% == Dividing the data splits amongst different workers.
for idxWorker = 1:numWorkers
    workerSplitList{idxWorker} = idxWorker : numWorkers : numSplits;
end

resultSplitWorker = cell(numWorkers,1);

parfor idxWorker = 1:numWorkers
% for idxWorker = 1:numWorkers
    disp('idxWorker')
    idxWorker
    numSplitsPerWorker = numel(workerSplitList{idxWorker}); % A count of number of splits that are assigned to each worker.
    
    localResultSplitWorker = cell(1, numSplitsPerWorker);
    
    for k=1:numSplitsPerWorker
        splitNum =  workerSplitList{idxWorker}(k);
        corr_bySigUnits_gTruth = zeros(numel(allSigUnits),1);
        corr_byOrder_gTruth = zeros(numel(allSigUnits),numel(nbs));
        pred_byOrder = cell(1,numel(nbs));
        
        if(debug)
            [idxWorker splitNum]
        end
        
        currData = modelData{splitNum} ;
        trainData  = currData{1};
        testData  = currData{2};
        avvasiTestMOS  = currData{3};
        gTruthTestMOS  = currData{4};
        
        hobfieldTestMOS = hobfieldQoE(testData.InputData);
        
        testDataIn = iddata([],testData.InputData,1); %only input of testData
        
        for nbIdx = 1:numel(nbs)
            nb = nbs(nbIdx)*ones(1,numIn);
            nf = nb-1;
            
            pred_bySigUnits = zeros(numel(allSigUnits),numel(avvasiTestMOS));
            
            for unitsIdx = 1:numel(allSigUnits)
                numSigUnits = allSigUnits(unitsIdx);
                InputNL = sigmoidnet('NumberOfUnits',numSigUnits); %input nonlinearity
                InputNLs = [InputNL;InputNL;InputNL]; %same input nonlinearity for each of the 3 inputs
                OutputNL = sigmoidnet('NumberOfUnits',numSigUnits); %output nonlinearity
                
                
                try
                    if(debug)
                        disp(strcat('Running a model with the following parameters', num2str(nbs(nbIdx)), ',',num2str(numSigUnits)))
                    end
                    m = nlhw(trainData,[nb,nf,nk],InputNLs,OutputNL);
                    
                    p = sim(m,testDataIn);
                    
                    if(debug)
                        disp(strcat('Finished running a model with the following parameters', num2str(nbs(nbIdx)), ',',num2str(numSigUnits)))
                    end
                    
                    finalMOS = zeros(1,numel(avvasiTestMOS));
                    
                    %% == Extracting the predicted MOS scores.
                    for testIdx = 1:numel(avvasiTestMOS)
                        currTest = getexp(p,testIdx);
                        finalMOS(1,testIdx) = currTest.OutputData(end);
                    end
                    
                    %% == Saving the correlation values computed.
                    corr_bySigUnits_gTruth(unitsIdx,1) = corr(finalMOS',gTruthTestMOS');
                    pred_bySigUnits(unitsIdx,:) = finalMOS;
                catch
                    corr_bySigUnits_gTruth(unitsIdx,1) = -1;
                    pred_bySigUnits(unitsIdx,:) = -1*ones(1,numel(avvasiTestMOS));
                end
                
                
            end % Units
            corr_byOrder_gTruth(:,nbIdx) = corr_bySigUnits_gTruth;
            pred_byOrder{1,nbIdx} = pred_bySigUnits;
        end % nBIdx
        
        %% == Correlation between Avvasi Model and the GT scores.
        corrAvvasiGT = corr(gTruthTestMOS',avvasiTestMOS');
        
        %% == Correlation between Hobfield Model and the GT scores.
        corrHobfieldGT = corr(gTruthTestMOS',hobfieldTestMOS');
        
        %% == Saving the results of the
        localResultSplitWorker{k} = {splitNum; corr_byOrder_gTruth; pred_byOrder;corrAvvasiGT;corrHobfieldGT};
    end
    resultSplitWorker{idxWorker}= localResultSplitWorker;
end

fileSuffix = num2str(floor(now));

save(strcat('resultSplitWorker1_',fileSuffix,'.mat'),'resultSplitWorker');
[HWMedian,HWMean,avvasiMedian,avvasiMean,hobfieldMedian,hobfieldMean] = parsingResultMatrix(resultSplitWorker,numSplits,length(allSigUnits),length(nbs))
e = cputime - startTime