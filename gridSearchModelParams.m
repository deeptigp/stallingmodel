%Test Hammerstein-Wiener Model
clear all
clc

load('outWaveforms.mat');
load('vidStallData_noTrain.mat','stallWaveforms');
load('vidStallData_noTrain.mat','timeSincePrev');
load('vidStallData_noTrain.mat','numDelays');
load('vidStallData_noTrain.mat','videoGroups');

numVids = numel(outWaveforms);
numIn = 3;
nk = zeros(1,numIn);
nfs = 5;
nbs = 5;
models = cell(1,numVids);
perf = cell(1,numVids);
nUnits = [5:5:20];

for i = 3:numVids
    i %print out vid #
    if videoGroups(1,i)~=-1
        in1 = stallWaveforms{1,i};
        in2 = timeSincePrev{1,i};
        in3 = numDelays{1,i};
        inall = [in1, in2, in3];

        out = outWaveforms{1,i}; %no need to zero pad for non-ref videos
        
        z1 = iddata(out,in1,1);
        z2 = iddata(out,in2,1);
        z3 = iddata(out,in3,1);
        zall = iddata(out,inall,1);

        ms = cell(length(nUnits),nbs);
        fpes = zeros(length(nUnits),nbs);
       
        for j = 1:length(nUnits)
        InputNL = sigmoidnet('NumberOfUnits',nUnits(j)); %input nonlinearity
        InputNLs = [InputNL;InputNL;InputNL];
        OutputNL = sigmoidnet('NumberOfUnits',nUnits(j)); %output nonlinearity; piecewise linear better?
            for k = 1:nfs
                nf = k*ones(1,numIn);
                nb = k*ones(1,numIn);
                try
                    m = nlhw(zall,[nb,nf,nk],InputNLs,OutputNL);
                    ms{j,k} = m;
                    fpes(j,k) = m.EstimationInfo.FPE; %http://www.mathworks.com/help/ident/ref/fpe.html
                catch
                    ms{j,k} = -1;
                    fpes(j,k) = {-1};
                end
            end
            
        end
        models{1,i} = ms;
        perf{1,i} = fpes;
    end
    break;
end


%% To compute MDL, for a given value of r 

% Call dynamicStallModel %% Returns globalOutageRate
% call: dLen = computeMDL(globalOutageRate, r);

