function [vt,qhatt] = constructLinFilterOutput(ut,currMOS,r,tempb,tempv,tempf)
    T = numel(ut); %length of current video in seconds
    out1 = [currMOS*ones(1,r) zeros(1,T-r)];
    qhatt = zeros(T,1);
    vt = zeros(T,1);

    for k = 1:T
        if k<=r+1
            qhatt(k,1) = out1(k);
            vt(k,1) = (qhatt(k,1)-tempv(2))/tempv(1);
        else
            vt(k,1) = tempb'*ut(:,k-r:k)' + vt(k-r:k-1)'*tempf;
            qhatt(k,1) = tempv(1)*vt(k,1)+tempv(2); %eq(13)
        end
    end
end