function [TrainingByGroup, TestingByGroup, IndsByGroup] = generateTrainTestSplitPerGroup(trainFraction,numSplits)
%% Example call: [TrainingByGroup, TestingByGroup, IndsByGroup] = generateTrainTestSplitPerGroup(0.8,20);

load('vidStallData_noTrain.mat');

testFraction = 1 - trainFraction;

TrainingByGroup = cell(1,3);
TestingByGroup = cell(1,3);
IndsByGroup = cell(1,3);

for group = 1:3
    groupInds = find(videoGroups==group);
    IndsByGroup{1,group} = groupInds;
    
    numGroupVids = length(groupInds);
    trainSplit = ceil(trainFraction*numGroupVids);
%     testSplit = floor(testFraction*numGroupVids);
    
    TrainingNIter = [];
    TestingNIter = [];
    
    for itr = 1:numSplits
        ind = randperm(numGroupVids);
        ind_train = zeros(numGroupVids,1);
        ind_test = zeros(numGroupVids,1);
        ind_train(ind(1:trainSplit))=1;
        ind_test(ind(trainSplit+1:end))=1;
        
        TrainingNIter = [ TrainingNIter ind_train];
        TestingNIter = [TestingNIter ind_test];
        clear ind_train; clear ind_test;
    end
    
    TrainingByGroup{1,group} = TrainingNIter;
    TestingByGroup{1,group} = TestingNIter;
end

fileSuffix ='80-20_5splits';
saveTrainingFile=strcat('TrainingNIterByGroup',fileSuffix,'.mat');
saveTestFile=strcat('TestingNIterByGroup',fileSuffix,'.mat');
saveIndFile=strcat('IndicesByGroup',fileSuffix,'.mat');

save (saveTrainingFile,'TrainingByGroup');
save (saveTestFile,'TestingByGroup');
save (saveIndFile,'IndsByGroup');
end