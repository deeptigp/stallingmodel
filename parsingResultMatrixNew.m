function [HWMedian,HWMean,avvasiMedian,avvasiMean,hobfieldMedian,hobfieldMean] = parsingResultMatrixNew(resultSplitWorker,numSplits,numHiddenUnits,numNB)
    numWorkers = length(resultSplitWorker);
    
    avvasiIndex = 4;
    hobfieldIndex = 5;
    predictedCorrIndex = 2;

    avvasiCorr = [];
    hobfieldCorr = [];

    corrScores_bySplit = zeros(numHiddenUnits,numNB,numSplits);
    splitIndx  = 1;
    
    for i = 1:numWorkers
        splitsPerWorker = resultSplitWorker{i};
        for j = 1:length(splitsPerWorker)
            
            currSplit = splitsPerWorker{j};
            avvasiCorr = [avvasiCorr;currSplit{avvasiIndex}];
            hobfieldCorr = [hobfieldCorr;currSplit{hobfieldIndex}];

            %% predicted scores..
            corrScores_bySplit(:,:,splitIndx)  = currSplit{predictedCorrIndex};
            splitIndx = splitIndx + 1;
        end
    end

    HWMedian = median(corrScores_bySplit,3);
    HWMean = mean(corrScores_bySplit,3);
    avvasiMedian = median(avvasiCorr);
    avvasiMean = mean(avvasiCorr);
    hobfieldMedian = median(hobfieldCorr);
    hobfieldMean = mean(hobfieldCorr);
    
end