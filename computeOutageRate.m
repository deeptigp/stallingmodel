%Given the input (after applying the non-linearity) and the qhatt, computes
%outage rate (eqn. 16) and gradient of U ( dU as defined in (17))
%Inputs: Ut.
%epsilon ( computed using confidence intervals)
%groundTruth MOS
%
function [Unu,GUnu,predictedScores] = computeOutageRate(input,epsilon,groupMOS,temptheta,modelParamsLength,r,nu,debug)
    
    %Extract model params.
    
    [tempv,tempw,tempb,tempf] = extractIndividualModelParams(temptheta,modelParamsLength); 
   
    GUnu = [];
    Unu = [];
    predictedScores =[];
    numVideos = length(groupMOS);
    
%     videosToSampleForDebug = randperm(numVideos,5);
    perm = randperm(numVideos);
    videosToSampleForDebug = perm(1:5);
    for seq = 1:numVideos
        in1 = input{1,seq};
        ut = inputNonLinearity(in1,tempw);
        currMOS = groupMOS(seq);
        epsilon_seq = epsilon(seq);
        
        [vt,qhatt] = constructLinFilterOutput(ut,currMOS,r,tempb,tempv,tempf);
        if(debug == 1 && ~isempty(find(videosToSampleForDebug == seq)))    
            plotInOut(in1,ut,vt,qhatt,seq);
        end
        
        dev = qhatt(end) - currMOS;
        Unu = [Unu sigmoid(dev,nu,-epsilon_seq*nu,0,1) + (1-sigmoid(dev,nu,epsilon_seq*nu,0,1))]; %eq(15)
        GUnu= [GUnu nu*(sigmoid(dev,nu,-epsilon_seq*nu,0,1)*(1-sigmoid(dev,nu,-epsilon_seq*nu,0,1))-sigmoid(dev,nu,epsilon_seq*nu,0,1)*(1-sigmoid(dev,nu,epsilon_seq*nu,0,1)))]; 
        predictedScores =[predictedScores qhatt(end)];
    end
  %  [Unu ; GUnu ; predictedScores]
end