nbs = [ 2 3 4 ];
numIn = 3;
nk = zeros(1,numIn);

allSigUnits = [10 20 30];
numSplits = 2;

models = cell(1,numel(allSigUnits));

corr_bySplit_avvasi_gTruth = zeros(1,numSplits);

corr_bySigUnits_gTruth = zeros(numel(allSigUnits),1);
corr_byOrder_gTruth = zeros(numel(allSigUnits),numel(nbs));
corr_bySplit_gTruth = cell(1,numSplits);

% pred_bySigUnits = zeros(numel(allSigUnits),numel(testMOS));
pred_byOrder = cell(1,numel(nbs));
pred_bySplit = cell(1,numSplits);


for splitNum = 1:numSplits
    splitNum
    [trainData, testData, avvasiTestMOS, gTruthTestMOS] = generateTrainTestData(splitNum);
    testDataIn = iddata([],testData.InputData,1); %only input of testData
    
    for nbIdx = 1:numel(nbs)
        nb = nbs(nbIdx)*ones(1,numIn);
        nf = nb + 1;
        
        pred_bySigUnits = zeros(numel(allSigUnits),numel(avvasiTestMOS));
        
        for unitsIdx = 1:numel(allSigUnits)
            numSigUnits = allSigUnits(unitsIdx);
            InputNL = sigmoidnet('NumberOfUnits',numSigUnits); %input nonlinearity
            InputNLs = [InputNL;InputNL;InputNL]; %same input nonlinearity for each of the 3 inputs
            OutputNL = sigmoidnet('NumberOfUnits',numSigUnits); %output nonlinearity
            
            try
                m = nlhw(trainData,[nb,nf,nk],InputNLs,OutputNL);
                models{1,unitsIdx} = m;
                p = sim(m,testDataIn);
                finalMOS = zeros(1,numel(avvasiTestMOS));
                
                for testIdx = 1:numel(avvasiTestMOS)
                    currTest = getexp(p,testIdx);
                    finalMOS(1,testIdx) = currTest.OutputData(end);
                end
                
                corr_bySigUnits_gTruth(unitsIdx,1) = corr(finalMOS',gTruthTestMOS')
                pred_bySigUnits(unitsIdx,:) = finalMOS;
                
            catch
                models{1,unitsIdx} = -1;
                corr_bySigUnits_gTruth(unitsIdx,1) = -1;
                pred_bySigUnits(unitsIdx,:) = -1*ones(1,numel(avvasiTestMOS));
            end
            
        end
        
        corr_byOrder_gTruth(:,nbIdx) = corr_bySigUnits_gTruth;
        pred_byOrder{1,nbIdx} = pred_bySigUnits;
    end
    
    corr_bySplit_avvasi_gTruth = [corr_bySplit_avvasi_gTruth corr(avvasiTestMOS',gTruthTestMOS')];
    
    %%== Compute the correlation between Avvasi's scores and ground
    %%truth MOS scores === %%
    
    corr_bySplit_gTruth{1,splitNum} = corr_byOrder_gTruth;
    pred_bySplit{1,splitNum} = pred_byOrder;
end

resultsByGroup = {corr_bySplit_gTruth;pred_bySplit;corr_bySplit_avvasi_gTruth};
