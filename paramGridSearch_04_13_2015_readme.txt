paramGridSearch_04_13_2015.mat:
- numSplits = 5; %number of splits
- nbs = [2, 3, 4] %(nf = nb+1)
- allSigUnits = [5,10,15,20] % # units in sigmoid
- corr_bySplit = pearson correlation;
		1x5 cell, 1 cell for each split;
		in each cell: 4x3 matrix -> 4 sig units; 3 nbs
- trainData, testData, trainMOS, testMOS
- pred_bysplit = predicted waveforms;
		1x5 cell, 1 cell for each split;
		for each split: 1x3 cell for each nb
		for each nb: 4x28 matrix -> 4 sig units; 28 predictions for each of 28 test videos