function [trainData, testData,avvasiTestMOS, gTruthTestMOS] = generateTrainTestData(splitNum)
%% Which train/test split to choose. 
    load('vidStallData_noTrain_exp050_exp020_exp00.mat'); %%%new%%%
    load('stallDensity.mat');
    load('outWaveforms.mat');
    load('DQS_MOS_Ref_scores.mat');
    
    %%Training Data.
    load('TrainingNIter80-20allGroups.mat');
    load('TestingNIter80-20allGroups.mat');
    
    DQS = dqsScores;
    
    avvasiMOS =[];
    
    for i = 1:length(outWaveforms)
        o = outWaveforms{1,i};
        avvasiMOS = [avvasiMOS  o(end)];
        outWaveforms{1,i} = scaleOutWaveform(o,DQS(i));
    end
    
    %%== We extract the MOS values that are assigned to each video. === 
%     avvasiMOS = avvasiMOS -(5-refScores);
    
    TrainingNIter = TrainingNIter(:,splitNum);
    TestingNIter = TestingNIter(:,splitNum);

    
    numTrainData = sum(TrainingNIter);
    numTestData = sum(TestingNIter);
    
    
    trainDataX = cell(1,numTrainData);
    trainDataY = cell(1,numTrainData);
    
    testDataX  = cell(1,numTestData);
    testDataY  = cell(1,numTestData);
    
    trainIndxs = find(TrainingNIter);
    testIndxs = find(TestingNIter);
    
    trainMOS = DQS(trainIndxs);
    gTruthTestMOS = DQS(testIndxs);
    
    avvasiTestMOS = avvasiMOS(testIndxs);
    
    %%Training Data
    for i = 1:numTrainData
        indx = trainIndxs(i);
        
        in1 = stallWaveforms{1,indx}; %%%new%%%
        in2 = timeSincePrev{1,indx};
        in3 = numDelays{1,indx};
        in4 = stallDensity{1,indx};
%         in1 = stallWaveforms{1,indx};
%         in2 = timeSincePrev{1,indx};
%         if ~isempty(find(isnan(in1))) || ~isempty(find(isnan(in2))) || ~isempty(find(isnan(in3)))
%             a = 'debug';
%         end
        
%         trainDataX{i} = [in1, in2, in3]; %%%new%%%
        trainDataX{i} = [in1, in2, in3, in4];
        trainDataY{i} = outWaveforms{1,indx};     
    end
    
    
    for i = 1:numTestData
        indx = testIndxs(i);
        
        in1 = stallWaveforms{1,indx}; %%%new%%%
        in2 = timeSincePrev{1,indx};
        in3 = numDelays{1,indx};
        in4 = stallDensity{1,indx};
%         in1 = stallWaveforms{1,indx};
%         in2 = timeSincePrev{1,indx};
        out = outWaveforms{1,indx};

%         testDataX{i} = [in1, in2, in3]; %%%new%%%
        testDataX{i} = [in1, in2, in3, in4];
        testDataY{i} = outWaveforms{1,indx};
    end
    
    trainData = iddata(trainDataY,trainDataX,1);
    testData = iddata(testDataY,testDataX,1);
end