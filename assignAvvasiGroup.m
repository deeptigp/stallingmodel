%assign avvasi group labels
%Divide videos into 3 categories:
% Initial category: videos having only the initial stall and nothing else.
% Single category: (the default initial stall) + 1 stall
% Multiple category: (the default initial stall) + 2 or more stalls 

clear all
load('vidStallData_noTrain.mat');

numVids = numel(names);
avvasiGroup = zeros(1,numVids);

countNoInit = 0; %just to make sure all the videos have an initial stall
for i = 1:numVids
    currStallInfo = stallLocs_s{1,i};
    if ~isequal(currStallInfo,-1)
        currStallInfo = currStallInfo(1:end-1,1);
        switch numel(currStallInfo)
            case 1
                avvasiGroup(1,i) = 1; %Initial category
            case 2
                avvasiGroup(1,i) = 2; %Single category
            otherwise
                avvasiGroup(1,i) = 3; %Multiple category
        end
        if currStallInfo(1) ~= 0
            countNoInit = countNoInit + 1;
        end
    end          
end
    