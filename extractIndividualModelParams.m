function [v, w, b, f] = extractIndividualModelParams(theta,modelParamsLength)
    lenV = modelParamsLength(1);
    lenW = modelParamsLength(2);
    lenB = modelParamsLength(3);
    lenF = modelParamsLength(4);
    
    
    v = theta(1:lenV);
    
    w=theta(lenV+1:lenV+lenW);
    
    b=theta(lenV+lenW+1:lenV+lenW+lenB);
    
    f=theta(lenV+lenW+lenB+1:length(theta));
end