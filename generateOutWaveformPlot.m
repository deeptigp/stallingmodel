load('outWaveforms.mat');
load('DQS_MOS_Ref_scores.mat');

out7 = outWaveforms{1,7};
out7scaled = scaleOutWaveform(out7,dqsScores(7));

figure(1);
plot(out7,'k--');
hold on
plot(out7scaled,'r');