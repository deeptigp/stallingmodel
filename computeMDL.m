function dLen = computeMDL(EU, r)
    load('vidStallData_noTrain.mat','vidLengths_s');
    sumLens = sum(vidLengths_s-r);
    T1 = (1+(2*r+1)*(log(sumLens)/sumLens))
    dLen = EU*T1;
end