function [trainData, testData, avvasiTestMOS, gTruthTestMOS] = generateTrainTestDataForGroup(splitNum,groupNum)
%% == Which train/test split to choose. == %%
    %splitNum = 1;
    load('vidStallData_noTrain.mat');
    load('outWaveforms.mat');
    
    %% == Training Data. == %%
    load('TrainingNIterByGroup80-20.mat');
    load('TestingNIterByGroup80-20.mat');
    load('IndicesByGroup80-20.mat');
    
    MOS = 5-DMOS;
    
    avvasiMOS =[];
    
    %% === The global video indexes === %%
    groupInds = IndsByGroup{groupNum};
    groupOutWaveforms = outWaveforms(groupInds);
    
    %START HEREEEE
    %% Extract the AvvasiMOS from the out wave forms.
    for i = 1:length(groupOutWaveforms)
        o = groupOutWaveforms{1,i};
        avvasiMOS = [avvasiMOS  ;o(end)];
    end
    
    % ====These vary in size, they depend on the total number of videos
    % that belong to each group. ======%%
    
    TrainingNIter = TrainingByGroup{1,groupNum};
    TestingNIter = TestingByGroup{1,groupNum};
    TrainingNIter = TrainingNIter(:,splitNum);
    TestingNIter = TestingNIter(:,splitNum);
    
    % Eg: for group 1, this will typically be 28 and 6. 
    numTrainData = sum(TrainingNIter);
    numTestData = sum(TestingNIter);
    
    trainDataX = cell(1,numTrainData);
    trainDataY = cell(1,numTrainData);
    
    testDataX  = cell(1,numTestData);
    testDataY  = cell(1,numTestData);
    
    %% == Here the global indxes are being extracted and stored in trainIndxs. === %%
    trainIndxs = groupInds(find(TrainingNIter));
    testIndxs = groupInds(find(TestingNIter));
    
    trainMOS = MOS(trainIndxs);
    gTruthTestMOS = MOS(testIndxs)'; %ground truth
    
    avvasiTestMOS = avvasiMOS(find(TestingNIter));
    
    % == Form the Training Data
    for i = 1:numTrainData
        indx = trainIndxs(i);
        
        in1 = stallWaveforms{1,indx};
        in2 = timeSincePrev{1,indx};
        in3 = numDelays{1,indx};
        
        trainDataX{i} = [in1, in2, in3];
        trainDataY{i} = outWaveforms{1,indx};     
    end
    
    % == Form the Testing Data
    for i = 1:numTestData
        indx = testIndxs(i);
        
        in1 = stallWaveforms{1,indx};
        in2 = timeSincePrev{1,indx};
        in3 = numDelays{1,indx};

        out = outWaveforms{1,indx};

        testDataX{i} = [in1, in2, in3];
%         testDataY{i} = ones(length(outWaveforms{1,indx}),1)*5;
        testDataY{i} = outWaveforms{1,indx};
    end
    
    trainData = iddata(trainDataY,trainDataX,1);
    testData = iddata(testDataY,testDataX,1);
%end