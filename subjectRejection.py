## Author: Deepti Ghadiyaram. 01/18/2015
## Desc: This script reads a CSV file which has 3 columns <userId, videoname, user rating> and identifies outliers.
## Ref: [1] ITU-R Recommendation BT.500-11, Methodology for the subjective assessment of the quality of television pictures, International Telecommunications Union, Tech. Rep.

#!/usr/bin/python
import csv
import math

## From a set of indices passed as input parameter, find the right user's index.
def findUserId(globalIndexList):
	allUIndices = []
	uIds = [userIds[x] for x in globalIndexList]
	for u in uIds:
		tempIndex=[indx for indx, val in enumerate(uniqueUserIds) if u == val]
		allUIndices = allUIndices + tempIndex
	return allUIndices

## The main subject rejection script.
def subjectReject():
	P = [0]*len(uniqueUserIds)
	Q = [0]*len(uniqueUserIds)


	#Computing MOS, Standard Deviation, m2, m4 
	for uniqueVName in uniqueVideoNames:
		vIndices= [i for i,s in enumerate(videoNames) if(s==uniqueVName)]
		
		N = len(vIndices)
		allURatings= [float(userRatings[v]) for v in vIndices]
		
		meanVal = sum(allURatings)/N
		
		# For Std. dev, beta
		squaredDiff= [pow((v-meanVal),2) for v in allURatings]
		quadDiff= [pow((v-meanVal),4) for v in allURatings]
		
		stdDev= math.sqrt(sum(squaredDiff)/(N-1))	

		# The two terms for beta..
		m2Temp = sum(squaredDiff)/N
		m4Temp = sum(quadDiff)/N

		beta = m4Temp/(m2Temp*m2Temp)


		##Subject Rejection ## The 4 conditions mentioned in the [1]
		if(2<=beta<=4):
			#These indices are relative to the array allURatings
			relUIndex = [uIndx for uIndx, val in enumerate(allURatings) if (val >= (meanVal + 2*stdDev))]
			globalIndices = [vIndices[u] for u in relUIndex] 
			allUIndices = findUserId(globalIndices) # User indices
			
			#Set P
			if allUIndices:
				for u in allUIndices:
					P[u]=P[u]+1

			relUIndex = [uIndx for uIndx, val in enumerate(allURatings) if (val<= (meanVal - 2*stdDev))]
			globalIndices = [vIndices[u] for u in relUIndex]
			allUIndices = findUserId(globalIndices)
			
			#Set Q
			if allUIndices:
				for u in allUIndices:
					Q[u]=Q[u]+1
		else:
			relUIndex = [uIndx for uIndx, val in enumerate(allURatings) if (val>= (meanVal + math.sqrt(20)*stdDev))]
			globalIndices = [vIndices[u] for u in relUIndex]
			allUIndices = findUserId(globalIndices)
			#Set P
			if allUIndices:
				for u in allUIndices:
					P[u]=P[u]+1


			relUIndex = [uIndx for uIndx, val in enumerate(allURatings) if (val<= (meanVal - math.sqrt(20)*stdDev))]
			globalIndices = [vIndices[u] for u in relUIndex]
			allUIndices = findUserId(globalIndices)
			#Set Q
			if allUIndices:
				for u in allUIndices:
					Q[u]=Q[u]+1

		MOS.append(meanVal)
		stdDeviation.append(stdDev)
		
		deltaConf.append(1.96*stdDev/math.sqrt(N))

	numTestVideos = len(uniqueVideoNames)*1.0
	for i in range(len(P)):
		if P[i] + Q[i] !=0:
		#	print (P[i]+Q[i])/numTestVideos, abs(((P[i]*1.0-Q[i])/(P[i]+Q[i])*1.0))
			if (((P[i]+Q[i])/numTestVideos) > 0.05) and (abs((P[i]-Q[i])/(P[i]+Q[i])*1.0) < 0.3):
				print uniqueUserIds[i]	



#Change the file name here to identify the outliers in a different file.
fHandler = file('device_a.csv', 'rb')
reader = csv.reader(fHandler, delimiter=' ', quotechar='|')

#Initializations
userIds = []
videoNames = []
userRatings = []
MOS = []
stdDeviation = []
deltaConf = []
beta = []

##Subject Rejection
P = []
Q = []

#Reading and storing data from the CSV file. 
for row in reader:
	sData= row[0].split(',')
	userIds.append(sData[0])
	videoNames.append(sData[1])
	userRatings.append(sData[2])

#Removing the header information
videoNames = videoNames[1:len(videoNames)]
userIds = userIds[1:len(userIds)]
userRatings = userRatings[1:len(userRatings)]

#Getting the unique data
uniqueVideoNames = list(set(videoNames))
uniqueUserIds = list(set(userIds))

#Call the subject rejection method.
subjectReject() ## This prints the userId of the subject to be rejected. 
