% generate continuous output for each sawtooth waveform using Avvasi's
% proposed model
function generateAvvasiOut()
    load('vidStallData_noTrain.mat');
    
    numVids = numel(DMOS);
    outWaveforms = cell(1,numVids);
    frustCount = 0;
    recovCount = 0;
 
 %   i = 11;%58;%4%67%17 %%Sample values to experiment with ( as was done
 %   in the html page).
    
    for vidId = 1:numVids
        currVidLength = vidLengths_s(vidId);
        currStallLocs_s = stallLocs_s{vidId}; %stall locations for current video
        currStallLengths_s = stallLengths_s{vidId}; %stall lengths for current video
        totalStallTime = 0;
        
        Q0 = 5;

       outWaveform = ones(currVidLength,1)*Q0;
        
        

        if currStallLocs_s == -1 
            outWaveforms{1,vidId} = outWaveform;
        else       
            t = 0;
            stallCount = 1;
            nextStallT = currStallLocs_s(stallCount) + totalStallTime;
            currStallLength = currStallLengths_s(stallCount);
            totalStallTime = totalStallTime + currStallLength;
            
            while t < currVidLength
                if(t~=0) %% TODO : Fix this crappy snippet later
                    Q0 = outWaveform(t+1);
                end
                if t == nextStallT %% Frustration period
                     frustCount = frustCount+1;
                     dqs = interruptDQS(Q0,frustCount,currStallLength); % Returns the output during the whole of the stall region.
                     outWaveform(t+2:t+1+currStallLength) = dqs;
                     t=t+currStallLength;
                     stallCount = stallCount+1;
                     nextStallT = currStallLocs_s(stallCount) + totalStallTime;
                     currStallLength = currStallLengths_s(stallCount);
                     totalStallTime = totalStallTime + currStallLength; 
                     
                elseif (t < nextStallT) %% Recovery period...
                    recovCount = recovCount+1;
                    if(currStallLocs_s(stallCount) ~=1000)
                        recoveryPeriod = nextStallT-t;
                    else
                        recoveryPeriod = currVidLength-t;
                    end
                    
                    dqs = playbackDQS(Q0,recovCount,recoveryPeriod); % Returns the output during the whole of the stall region.
                    outWaveform(t+2:t+1+recoveryPeriod) = dqs;
                    t=t+recoveryPeriod;
                end
            end   %while   
            plot(outWaveform);
            axis([1,vidLengths_s(vidId),0,6]);
            outWaveform(end)
        end % if
            outWaveforms{1,vidId} = outWaveform;
        end %for
        save('outWaveforms.mat','outWaveforms');
end %func.


function sol = interruptDQS(Q0,interruptCnt, stLen)
    if(interruptCnt == 1) % Only initial
        T1 = 1;
        T2 = 2;
        a = 1.3;
        thr = 89;
    elseif(interruptCnt == 2) % Only single.
         T1 = 0;
        T2 = 0;
        a = 1.6;
        thr = 28;
    elseif(interruptCnt) % Multiple.
        T1 = 0;
        T2 = 0;
        a = 0.3;
        thr = 116;
    end
    
     %% Computing m.
    m = ((Q0 - 1 -a) / (thr - T2));
        
    sol = ones(stLen,1);
    t = 0;   
    
    while (t < stLen)
        
        if t<T1
            dqs = Q0;
            
        elseif t<=T2
            if(T1 == T2)
                dqs = Q0;
            else
                dqs = Q0 - a/2 * (1 + cos(pi*(t-T2)/(T2-T1)));
            end
        else
            dqs = Q0 - (a + m*(t - T2));
        end

        if dqs < 1
            sol(t+1) = 1;
        else
            sol(t+1) = dqs;
        end
        
        t = t+1;
    end
end

function sol = playbackDQS(Q0,recovCnt, recovLen)
    %% We have a video belonging to one of the three categories - single, multiple and those with just initial.
    if(recovCnt == 1) % Only initial
        T1 = 0;
        T2 = 3;
        a = 1.0;
        thr = 140;
    elseif(recovCnt == 2) % Only single.
        T1 = 0;
        T2 = 2;
        a = 1.5;
        thr = 390;
    else % Multiple.
        T1 = 0;
        T2 = 0;
        a = 0.0;
        thr = 294;
    end
    
     %% Computing m.
    m = ((5 - Q0 -a) / (thr - T2));
        
    sol = ones(recovLen,1);
    t = 0;   
        
    while (t < recovLen)
        
        if t<T1
            dqs = Q0;
        elseif t<=T2
             if(T1 == T2)
                dqs = Q0;
            else
            dqs = Q0 + a/2 * (1 + cos(double(3.14159*(t-T2)/(T2-T1))));
             end
        else
            dqs = Q0 + (a + m*(t - T2));
        end

        if dqs > 5
            sol(t+1) = 5;
        else
            sol(t+1) = dqs;
        end
        
        t = t+1;
    end
end