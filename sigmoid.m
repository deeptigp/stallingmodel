function sol = sigmoid(x,a,b,c,d)
sol = c + d./(1 + exp(-(a*x + b)));