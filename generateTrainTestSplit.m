function [TrainingNIter, TestingNIter] = generateTrainTestSplit(trainFraction,numSplits)
%% Example call: [TrainingNIter, ValidNIter, TestingNIter] = generateTrainTestSplit(0.7,0.1,10);

load('vidStallData_noTrain.mat');

distortedVideoIndxs = find(videoGroups~=-1);

numDistortedVids = length(distortedVideoIndxs);

numTotalVids = length(videoGroups);

ind_others = zeros(numTotalVids,1);
ind_others(find(videoGroups==-1)) = 1;

testFraction = (1-trainFraction);

% Test and Training split
trainSplit = ceil(trainFraction*numDistortedVids);
testSplit = ceil(testFraction*numDistortedVids);

TrainingNIter= [];
TestingNIter= [];

for itr = 1:numSplits
    ind = randperm(numDistortedVids);
    ind = ind(1:trainSplit);
    
    ind_train = zeros(numTotalVids,1);
    ind_test = zeros(numTotalVids,1);
        
    ind_train(distortedVideoIndxs(ind))=1;
    
    ind_test = 1- (ind_train | ind_others);
    
   
    TrainingNIter = [ TrainingNIter ind_train];
    TestingNIter = [TestingNIter ind_test];

    
    clear ind_train; ind_test;
end
fileSuffix ='80-20';
groupType = 'allGroups';

saveTrainingFile=strcat('TrainingNIter',fileSuffix,groupType,'.mat');
saveTestFile=strcat('TestingNIter',fileSuffix,groupType,'.mat');
%saveValFile=strcat('ValidNIter',fileSuffix,'.mat');

save (saveTrainingFile,'TrainingNIter');
save (saveTestFile,'TestingNIter');
%save (saveValFile,'ValidNIter');
end