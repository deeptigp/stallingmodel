%compute MSE and number of errors
clear all

load('outWaveforms.mat');
load('vidStallData_noTrain.mat');

RDMOS = 5-DMOS;
eps = (CIhigh-CIlow)/2;

numErr = 0;

numVids = numel(names);
sqErr = zeros(1,numVids);
for i = 1:numVids
    currOut = outWaveforms{1,i};
    outQ = currOut(end);
    trueQ = RDMOS(1,i);
    sqErr(1,i) = (outQ-trueQ)^2;
    if abs(outQ-trueQ)>2*eps
        numErr = numErr + 1;
    end
end

mse = mean(sqErr)
numErr