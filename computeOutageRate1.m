%Given the input (after applying the non-linearity) and the qhatt, computes
%outage rate (eqn. 16) and gradient of U ( dU as defined in (17))
%Inputs: Ut.
%epsilon ( computed using confidence intervals)
%groundTruth MOS
%
function [Unu,GUnu,predictedScores] = computeOutageRate1(input,epsilon,groupMOS,temptheta,modelParamsLength,r,nu)
    
    %Extract model params.
    
    [tempv,tempw,tempb,tempf] = extractIndividualModelParams(temptheta,modelParamsLength); 
   
    GUnu = [];
    Unu = [];
    predictedScores =[];
    numVideos = length(groupMOS);
    
    
    for seq = 1:numVideos
        in1 = input{1,seq};
        ut = inputNonLinearity(in1,tempw);
       
        currMOS = groupMOS(seq);
        epsilon_seq = epsilon(seq);
        
        [vt,qhatt] = constructOutput(ut,currMOS,r,tempb,tempv,tempf);
        if(seq == 2)
            subplot(2,1,1)
            plot(ut)
            subplot(2,1,2)
            plot(vt)
            hold on
            plot(qhatt,'r');
        end
        dev = qhatt(end) - currMOS;
        Unu = [Unu sigmoid(dev,nu,-epsilon_seq*nu,0,1) + (1-sigmoid(dev,nu,epsilon_seq*nu,0,1))]; %eq(15)
        GUnu= [GUnu nu*(sigmoid(dev,nu,-epsilon_seq*nu,0,1)*(1-sigmoid(dev,nu,-epsilon_seq*nu,0,1))-sigmoid(dev,nu,epsilon_seq*nu,0,1)*(1-sigmoid(dev,nu,epsilon_seq*nu,0,1)))]; 
        predictedScores =[predictedScores qhatt(end)];
    end
  %  [Unu ; GUnu ; predictedScores]
end