clear all

load('vidStallData.mat');
MOS = 5-DMOS;
eps = (CIhigh - CIlow)/2;

groupInds = find(videoGroups==1); %videos in the short stall group
groupStallWaveforms = stallWaveforms(groupInds);
groupNumDelays = numDelays(groupInds);
groupTimeSincePrev = timeSincePrev(groupInds);
groupMOS = MOS(groupInds);
groupEps = eps(groupInds);
groupCIhigh = groupMOS+groupEps;
groupCIlow = groupMOS-groupEps;

r = 6;

nf = r;
nb = nf;
t = 1;
m = numel(groupInds);
w = [0.5 -2 -1 6]'; %[0.115 2.3 -150 180]';
b = 0.01*ones(nb+1,1); %0.03*ones(nb+1,1);
f = 0.01*ones(nf,1); %0.008*ones(nf,1);
v = [0.5 2.5]';

dimtheta = length(w) + length(b) + length(f) + length(v);

shrink = 1;
beta = 0.7;
alpha = 0.1;

itr = 0;
preitr = 0;
cost = 1000; %
newcost = 10000; %
precost = 1000;
nu = 0.8;
avgOR = 1;
preOR = 10; %
p = 0;

while nu<20
    preOR = avgOR;
    t = 1;
    cost = 1000;
    newcost = 10;
    while cost-newcost > 1e-5
        itr = itr + 1;
        gradient = zeros(dimtheta,1);
        cost = 0;
        for seq = 1:m
            in1 = groupStallWaveforms{1,seq};
            in2 = groupNumDelays{1,seq};
            in3 = groupTimeSincePrev{1,seq};
            T = numel(in1); %length of current video in seconds
            
            currMOS = groupMOS(seq);
            out1 = [currMOS*ones(1,r) zeros(1,length(in1)-r)];
            
            X = zeros(4,T);
            qhatt = zeros(T,1);
            vt = zeros(T,1);
            
            X(1,:) = [sigmoid(in1,w(1),w(2),0,w(4)).*(1-sigmoid(in1,w(1),w(2),0,1)).*in1]';
            X(2,:) = [sigmoid(in1,w(1),w(2),0,w(4)).*(1-sigmoid(in1,w(1),w(2),0,1))]';
            X(3,:) = 1;
            X(4,:) = [sigmoid(in1,w(1),w(2),0,1)]';
            ut = sigmoid(in1',w(1),w(2),w(3),w(4)); %eq(12)
            epsilon = shrink*(groupCIhigh(seq)-groupCIlow(seq));
            
            for k = 1:length(out1)
                if k <= max(nf,nb)+1
                    qhatt(k,1) = out1(k);
                    vt(k,1) = (qhatt(k,1)-v(2))/v(1);
                else
                    vt(k,1) = b'*ut(:,k-nb:k)' + vt(k-nf:k-1)'*f(1:nf); %eq(11)
                    qhatt(k,1) = v(1)*vt(k,1)+v(2); %eq(13)
                end
                %don't need to compute devation from true value at each
                %time instance
            end
            dev = qhatt(end) - currMOS;
            Unu(1,seq) = sigmoid(dev,nu,-epsilon*nu,0,1) + (1-sigmoid(dev,nu,epsilon*nu,0,1)); %eq(15)
            GUnu(1,seq)=nu*(sigmoid(dev,nu,-epsilon*nu,0,1)*(1-sigmoid(dev,nu,-epsilon*nu,0,1))-sigmoid(dev,nu,epsilon*nu,0,1)*(1-sigmoid(dev,nu,epsilon*nu,0,1))); %gradient of U_nu [SHOULD THIS BE SCALAR? [!!!]]
            temp_Gzv = zeros(length(v),T);
            temp_Gv = zeros(length(v),T);
            for k = max(nf,nb)+1:T
                temp_Gzv(:,k) = temp_Gzv(:,k-nf:k-1)*f(1:nf);
                temp_Gv(:,k) = cat(1,vt(k,1),1) + temp_Gzv(:,k);
            end
            temp_Gzw = zeros(length(w),T);
            temp_Gw = zeros(length(w),T);
            for k = max(nf,nb)+1:T
                temp_Gzw(:,k) = X(:,k-nb:k)*b+temp_Gzw(:,k-nf:k-1)*f(1:nf); %eq(28)
                temp_Gw(:,k) = v(1)*temp_Gzw(:,k);
            end
            temp_Gzb = zeros(length(b),T);
            temp_Gb = zeros(length(b),T);
            for k = max(nf,nb)+1:T
                temp_Gzb(:,k) = ut(:,k-nb:k)'+temp_Gzb(:,k-nf:k-1)*f(1:nf);
                temp_Gb(:,k) = v(1)*temp_Gzb(:,k);
            end
            temp_Gzf = zeros(length(f),T);
            temp_Gf = zeros(length(f),T);
            for k = max(nf,nb)+1:T
                temp_Gzf(:,k) = vt(k-nf:k-1) + temp_Gzf(:,k-nf:k-1)*f;
                temp_Gf(:,k) = v(1)*temp_Gzf(:,k);
            end
            gradient = gradient + 2*cat(1,temp_Gv,temp_Gw,temp_Gb,temp_Gf)*(GUnu(1,seq)*ones(T,1)); %GUnu needs to have dimensions Tx1 [!!!]
        end
        gradient = gradient/m;
        Eapx_nu = mean(Unu); %E^apx_nu; eq(16)
        cost = Eapx_nu;
        
        omega = 1;
        theta = cat(1,v,w,b,f);
        tempf = 5*ones(length(f),1);
        
        while(max(abs(roots(cat(1,1,-flipud(tempf)))))>1)
            omega = beta*omega;
            temptheta = theta - omega * gradient;
            tempf = temptheta(length(v)+length(w)+length(b)+1:length(theta));
        end
        
        tempv = temptheta(1:length(v));
        tempw=temptheta(length(v)+1:length(v)+length(w));
        tempb=temptheta(length(v)+length(w)+1:length(v)+length(w)+length(b));
        tempf=temptheta(length(v)+length(w)+length(b)+1:length(theta));
        
        newcost = 0;
        for seq = 1:m
            in1 = groupStallWaveforms{1,seq};
            in2 = groupNumDelays{1,seq};
            in3 = groupTimeSincePrev{1,seq};
            T = numel(in1); %length of current video in seconds
            
            ut = sigmoid(in1',tempw(1),tempw(2),tempw(3),tempw(4)); %line 133 in code annotations
            epsilon = shrink*(groupCIhigh(seq)-groupCIlow(seq));
            
            currMOS = groupMOS(seq);
            out1 = [currMOS*ones(1,r) zeros(1,length(in1)-r)];
            
            for k = 1:T
                if k <= max(nf,nb)+1
                    qhatt(k,1) = out1(k);
                    vt(k,1) = (qhatt(k,1)-tempv(2))/tempv(1);
                else
                    vt(k,1) = tempb'*ut(:,k-nb:k)' + vt(k-nf:k-1)'*tempf;
                    qhatt(k,1) = tempv(1)*vt(k,1)+tempv(2); %eq(13)
                end
                %don't need to compute devation from true value at each
                %time instance
            end
            dev = qhatt(end) - currMOS;
            Unu(1,seq) = sigmoid(dev,nu,-epsilon*nu,0,1) + (1-sigmoid(dev,nu,epsilon*nu,0,1)); %eq(15)
        end
        newcost = mean(Unu);
        
        while newcost > cost - alpha*omega*gradient'*gradient
            if omega < 1e-30
                break;
            end
            omega = beta * omega;
            temptheta = theta - omega*gradient;
            tempv = temptheta(1:length(v));
            tempw=temptheta(length(v)+1:length(v)+length(w));
            tempb=temptheta(length(v)+length(w)+1:length(v)+length(w)+length(b));
            tempf=temptheta(length(v)+length(w)+length(b)+1:length(theta));
            
            newcost = 0;
            for seq = 1:m
                in1 = groupStallWaveforms{1,seq};
                in2 = groupNumDelays{1,seq};
                in3 = groupTimeSincePrev{1,seq};
                T = numel(in1); %length of current video in seconds
                
                ut = sigmoid(in1',tempw(1),tempw(2),tempw(3),tempw(4));
                epsilon = shrink*(groupCIhigh(seq)-groupCIlow(seq)); %2*epsilon in paper
                
                currMOS = groupMOS(seq);
                out1 = [currMOS*ones(1,r) zeros(1,length(in1)-r)];
                
                for k = 1:T
                    if k<=max(nf,nb)+1
                        qhatt(k,1) = out1(k);
                        vt(k,1) = (qhatt(k,1)-tempv(2))/tempv(1);
                    else
                        vt(k,1) = tempb'*ut(:,k-nb:k)' + vt(k-nf:k-1)'*tempf;
                        qhatt(k,1) = tempv(1)*vt(k,1)+tempv(2); %eq(13)
                    end
                end
                dev = qhatt(end) - currMOS;
                Unu(1,seq) = sigmoid(dev,nu,-epsilon*nu,0,1) + (1-sigmoid(dev,nu,epsilon*nu,0,1)); %eq(15)
            end
        end
        newcost = mean(Unu);
        optlog(itr) = newcost;
        v = tempv;
        w = tempw;
        b = tempb;
        f = tempf;
        %dom_t
        
        numerror = 0; %counts number badly-predicted final DMOS
        for seq = 1:m
            in1 = groupStallWaveforms{1,seq};
            in2 = groupNumDelays{1,seq};
            in3 = groupTimeSincePrev{1,seq};
            T = numel(in1); %length of current video in seconds
            
            ut = sigmoid(in1',tempw(1),tempw(2),tempw(3),tempw(4)); %eq(12) with new model params
            epsilon = shrink*(groupCIhigh(seq)-groupCIlow(seq)); %2*epsilon in paper
            
            currMOS = groupMOS(seq);
            out1 = [currMOS*ones(1,r) zeros(1,length(in1)-r)];
            
            for k = 1:T
                if k <= max(nf,nb) + 1
                    qhatt(k,1) = out1(k);
                    vt(k,1) = (qhatt(k,1)-v(2))/v(1);
                else
                    vt(k,1)=b'*ut(:,k-nb:k)'+vt(k-nf:k-1)'*f;
                    qhatt(k,1)=v(1)*vt(k,1)+v(2);
                end
                
%                 if qhatt(k,1) > currMOS + epsilon || qhat(k,1) < currMOS - epsilon
%                     numerror = numerror + 1;
%                 end
            end
            if qhatt(end) > currMOS + epsilon || qhatt(end) < currMOS - epsilon
                numerror = numerror + 1;
            end
        end
        if itr-preitr >= 500
            break;
        end
    end
    nu = nu * 1.2; %Algorithm 1, line 4
    preitr = itr; %previous # iterations
end

sigma2 = 0;
error = 0;
errorlog = zeros(1,m);
for seq = 1:m
    in1 = groupStallWaveforms{1,seq};
    in2 = groupNumDelays{1,seq};
    in3 = groupTimeSincePrev{1,seq};
    T = numel(in1); %length of current video in seconds
    
    ut = sigmoid(in1',w(1),w(2),w(3),w(4));
    
    currMOS = groupMOS(seq);
    out1 = [currMOS*ones(1,r) zeros(1,length(in1)-r)];
    
    qhatt = zeros(T,1);
    vt = zeros(T,1);
    
    for k = 1:T
        if k <= max(nf,nb) + 1
            qhatt(k,1) = out1(k);
            vt(k,1) = (qhatt(k,1)-v(2))/v(1);
        else
            vt(k,1) = b'*ut(:,k-nb:k)'+vt(k-nf:k-1)'*f;
            qhatt(k,1) = v(1)*vt(k,1)+v(2);
        end
    end
    if qhatt(end) > currMOS + epsilon || qhatt(end) < currMOS - epsilon
        error = error + 1;
        errorlog(seq) = 1;
    end
    % other stuff
end