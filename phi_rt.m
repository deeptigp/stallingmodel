function sol = phi_rt(q,r,t)
if t-r < 1
    sol = [zeros(r-t,1);q(1:t,1)];
else
    sol = q(t-r:t-1,1);
end