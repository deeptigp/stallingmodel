% generate continuous output for each sawtooth waveform using Avvasi's
% proposed model
clear all
load('vidStallData_noTrain.mat');

numVids = numel(DMOS);
outWaveforms = cell(1,numVids);

a = 1;
m = 0.01;

for i = 1:numVids
    currVidLength = vidLengths_s(i);
    currStallLocs_s = stallLocs_s{i}; %stall locations for current video
    currStallLengths_s = stallLengths_s{i}; %stall lengths for current video
    totalStallTime = 0;
    
    outWaveform = zeros(currVidLength+1,1);
    
    if currStallLocs_s == -1 %% TODO: We might need to fix this and make this as 5, in the case of reference videos.
        outWaveforms{1,i} = outWaveform;
    else
        numStalls = numel(currStallLocs_s)-1; %number of stalls in current video;
        
        %disregard stall locs of 1000 and stall lengths of 20s
        t = 0;
        stallCount = 1;
        
        currT1 = currStallLocs_s(stallCount) + totalStallTime;
        currStallLength = currStallLengths_s(stallCount);
        
        totalStallTime = totalStallTime + currStallLength;
        
        currT2 = currT1 + currStallLength;
        
        prevT2 = 0; %end of previous stall for modeling satisfaction during playback
        Q0 = 5;
        while t <= currVidLength
            if t == currT1 && t > 0 %start of stall
                Q0 = outWaveform(t);
            end
            if t < currT1
                if t == 32
                    debug = 'a';
                end
                outWaveform(t+1) = playbackDQS(t,Q0,prevT2,currT1,a,m);
            else %if t <= currT2
                outWaveform(t+1) = interruptDQS(t,Q0,currT1,currT2,a,m);
            end
            if t == currT2 %end of a stall
                Q0 = outWaveform(t+1);
                if stallCount < numStalls
                    stallCount = stallCount + 1;
                    prevT2 = currT2;
                    currT1 = currStallLocs_s(stallCount) + totalStallTime;
                    currStallLength = currStallLengths_s(stallCount);
                    totalStallTime = totalStallTime + currStallLength;
                    currT2 = currT1 + currStallLength;
                else %no more stalls
                    prevT2 = currT2;
                    currT1 = currVidLength+1;
                end
            end
            t = t + 1;
        end
        outWaveforms{1,i} = outWaveform;
    end
end

figure(1)
subplot(2,1,1)
plot(outWaveforms{1,3})
subplot(2,1,2)
plot(stallWaveforms{1,3})

figure(2)
subplot(2,1,1)
plot(outWaveforms{1,4})
subplot(2,1,2)
plot(stallWaveforms{1,4})