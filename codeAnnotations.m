%% BP algorithm for hemmerstein-wiener model identification
% suppose the input nonlinearity is a sigmoid 
% suppose the output nonlinearity is a linear function
% employ BP algorithm to find out OR-minimized solution
TT=300;
for nf=12
        
        nf
    % system order
    %nb=9;
    %nf=10;
    nb=nf;
    t=1;
    m=15;%number of experiments
    %Initialization
    w=[0.115 2.3 -150 180]';%parameters for sigmoid function y=d*sigmoid(ax+b)+c w=[a b c d]'; %betas in paper
    %w=[0.25,1.5,-50,80]';
    %w=[0.08,3,-35,100]';
    b=0.03*ones(nb+1,1);%[0.27035 -0.74981 1 -0.90256 0.38195]'; %b and f in equation (11)
    f=0.008*ones(nf,1);%flipud(p);
    v=[0.5 50]'; %gammas; linear output function (vs. non-linear output fcn in paper)

    % w=0.03*ones(3,1);
    % b=0.03*ones(nb,1);
    % f=0.008*ones(nf,1);
    % v=[0.4562 36.6853]';
    shrink=1;
    beta=0.7;
    alpha=0.1;
    seqind=[2:6,8:12,14:18];
    input=input_mos(1:TT,seqind)-50;%input of system
    output=MOS(:,seqind);
    CI_high=MOSCI_high(:,seqind);
    CI_low=MOSCI_low(:,seqind);
    gCI_high=1.8*(MOSCI_high(:,seqind)-MOS(:,seqind))+MOS(:,seqind);
    gCI_low=1.8*(MOSCI_low(:,seqind)-MOS(:,seqind))+MOS(:,seqind);
    % for variable w
    %% iterative search
    itr=0;
    preitr=0;
    cost=1000;
    newcost=10000;
    precost=1000;
    nu=0.8;
    avgOR=1;
    preOR=10;
    p=0;
    while nu<20
        preOR=avgOR;
        t=1;
        cost=1000;
        newcost=10;
        while cost-newcost>1e-5
            itr=itr+1;
            gradient=zeros(length(v)+length(w)+length(b)+length(f),1);
            cost=0;
            for seq=1:m

                %X=[input(:,seq).^0,input(:,seq).^1,input(:,seq).^2]';
                X(1,:)=[sigmoid(input(:,seq),w(1),w(2),0,w(4)).*(1-sigmoid(input(:,seq),w(1),w(2),0,1)).*input(:,seq)]'; %gradient of U wrt beta
                X(2,:)=[sigmoid(input(:,seq),w(1),w(2),0,w(4)).*(1-sigmoid(input(:,seq),w(1),w(2),0,1))]';
                X(3,:)=1;
                X(4,:)=[sigmoid(input(:,seq),w(1),w(2),0,1)]';
                tic
                U=sigmoid(input(:,seq)',w(1),w(2),w(3),w(4)); %u, equation (12)
                epsilon=shrink*(CI_high(:,seq)-CI_low(:,seq));
                for k=1:length(output)
                    
                    if k<=max(nf,nb)+1
                        Y(k,1)=output(k,seq);
                        Z(k,1)=(Y(k,1)-v(2))/v(1);
                    else
                        Z(k,1)=b'*U(:,k-nb:k)'+Z(k-nf:k-1)'*f(1:nf); %v in paper, equation (11)
                        Y(k,1)=v(1)*Z(k,1)+v(2); %qhat, equation (13)
                    end
                    toc
                    dev(k)=(Y(k)-output(k,seq));% deviation from true value 
                    Gerror(k)=nu*(sigmoid(dev(k),nu,-(nu*epsilon(k)+p),0,1)*(1-sigmoid(dev(k),nu,-(nu*epsilon(k)+p),0,1))-sigmoid(dev(k),nu,(nu*epsilon(k)+p),0,1)*(1-sigmoid(dev(k),nu,(nu*epsilon(k)+p),0,1))); %gradient of U_nu
                    error(k)=sigmoid(dev(k),nu,-(nu*epsilon(k)+p),0,1)+(1-sigmoid(dev(k),nu,(nu*epsilon(k)+p),0,1)); %U_nu
                end

                cost=cost+sum(error)/(length(output)*m); %equation (16), E^apx_nu
                temp_Gzv=zeros(length(v),length(MOS));
                temp_Gv=zeros(length(v),length(MOS));
                for k=max(nf,nb)+1:length(MOS)% calculate gradient %in paper: gradient of gammas
                    temp_Gzv(:,k)=temp_Gzv(:,k-nf:k-1)*f(1:nf); %recall: linear output function (vs. nonlinear in paper)
                    temp_Gv(:,k)=cat(1,Z(k,1),1)+temp_Gzv(:,k);
                end
                temp_Gzw=zeros(length(w),length(output));
                temp_Gw=zeros(length(w),length(output));
                for k=max(nf,nb)+1:length(MOS)% calculate gradient
                    temp_Gzw(:,k)=X(:,k-nb:k)*b+temp_Gzw(:,k-nf:k-1)*f(1:nf); %equation (28)
                    temp_Gw(:,k)=v(1)*temp_Gzw(:,k);
                end
                temp_Gzb=zeros(length(b),length(MOS));
                temp_Gb=zeros(length(b),length(MOS));
                for k=max(nf,nb)+1:length(MOS)% calculate gradient
                    temp_Gzb(:,k)=U(:,k-nb:k)'+temp_Gzb(:,k-nf:k-1)*f(1:nf); %gradient of Z (paper: v) wrt b
                    temp_Gb(:,k)=v(1)*temp_Gzb(:,k);
                end
                temp_Gzf=zeros(length(f),length(MOS));
                temp_Gf=zeros(length(f),length(MOS));
                for k=max(nf,nb)+1:length(MOS)% calculate gradient
                    temp_Gzf(:,k)=Z(k-nf:k-1)+temp_Gzf(:,k-nf:k-1)*f; %gradient of Z (paper: v) wrt f
                    temp_Gf(:,k)=v(1)*temp_Gzf(:,k);
                end
                gradient=gradient+2*cat(1,temp_Gv,temp_Gw,temp_Gb,temp_Gf)*Gerror';
            end
            gradient=gradient/(length(output)*m);
            % backtracking line search
            t=1; %in paper: omega
            theta=cat(1,v,w,b,f); %concatentating all parameters
            tempf=5*ones(length(f),1);
%             tempv=temptheta(1:length(v));
%             tempw=temptheta(length(v)+1:length(v)+length(w));
%             tempb=temptheta(length(v)+length(w)+1:length(v)+length(w)+length(b));
%             tempf=temptheta(length(v)+length(w)+length(b)+1:length(theta));
            %compute cost
            while (max(abs(roots(cat(1,1,-flipud(tempf)))))>1) %flipud: flip matrix in up-down direction; Algorithm 2, line 3
                %sigmoid(-20,tempw(1),tempw(2),tempw(3),tempw(4)*sum(tempb)/(1-sum(tempf))*v(1)+v(2))
                t=beta*t; %Algorithm 2, line 4
                temptheta=theta-t*gradient; %Algorithm 2, line 6
                tempf=temptheta(length(v)+length(w)+length(b)+1:length(theta));
            end
            %extracting specific parameters from within theta
            tempv=temptheta(1:length(v));
            tempw=temptheta(length(v)+1:length(v)+length(w));
            tempb=temptheta(length(v)+length(w)+1:length(v)+length(w)+length(b));
            tempf=temptheta(length(v)+length(w)+length(b)+1:length(theta));
            newcost=0;
            for seq=1:m
                %X=[input(:,seq).^0,input(:,seq).^1,input(:,seq).^2]';
                U=sigmoid(input(:,seq)',tempw(1),tempw(2),tempw(3),tempw(4));
                epsilon=shrink*(CI_high(:,seq)-CI_low(:,seq));
                for k=1:length(MOS) % use temp model params to compute error (U_nu) in equation (15)
                    if k<=max(nf,nb)+1
                        Y(k,1)=output(k,seq);
                        Z(k,1)=(Y(k,1)-tempv(2))/tempv(1);
                    else
                        Z(k,1)=tempb'*U(:,k-nb:k)'+Z(k-nf:k-1)'*tempf;
                        Y(k,1)=tempv(1)*Z(k,1)+tempv(2);
                        dev(k)=(Y(k)-output(k,seq));% deviation from true value 
                        error(k)=sigmoid(dev(k),nu,-(nu*epsilon(k)+p),0,1)+(1-sigmoid(dev(k),nu,(nu*epsilon(k)+p),0,1));
                    end
                end

                newcost=newcost+sum(error)/(length(output)*m); %equation (16)
            end

            while newcost>cost-alpha*t*gradient'*gradient %Algorithm 2, line 3
                if t<1e-30
                    break;
                end;
                 t=beta*t;
                 temptheta=theta-t*gradient;
                 tempv=temptheta(1:length(v));
                 tempw=temptheta(length(v)+1:length(v)+length(w));
                 tempb=temptheta(length(v)+length(w)+1:length(v)+length(w)+length(b));
                 tempf=temptheta(length(v)+length(w)+length(b)+1:length(theta));

                 newcost=0;
                 for seq=1:m
                    %X=[input(:,seq).^0,input(:,seq).^1,input(:,seq).^2]';
                    U=sigmoid(input(:,seq)',tempw(1),tempw(2),tempw(3),tempw(4));
                    epsilon=shrink*(CI_high(:,seq)-CI_low(:,seq));
                    for k=1:length(MOS)
                        if k<=max(nf,nb)+1
                            Y(k,1)=output(k,seq);
                            Z(k,1)=(Y(k,1)-tempv(2))/tempv(1);
                        else
                            Z(k,1)=tempb'*U(:,k-nb:k)'+Z(k-nf:k-1)'*tempf;
                            Y(k,1)=tempv(1)*Z(k,1)+tempv(2);
                            dev(k)=(Y(k)-output(k,seq));% deviation from true value 
                            error(k)=sigmoid(dev(k),nu,-(nu*epsilon(k)+p),0,1)+(1-sigmoid(dev(k),nu,(nu*epsilon(k)+p),0,1));
                        end
                    end
                 newcost=newcost+sum(error)/(length(output)*m);
                 end
            end %now have theta(j+1); Algorithm 2, line 6
            newcost;
            optlog(itr)=newcost;
            v=tempv;
            w=tempw;
            b=tempb;
            f=tempf;
            dom_t(nf,itr)=max(-3./log(abs(roots(cat(1,1,-flipud(f))))));
            for seq=1:m
                %X=[input(:,seq).^0,input(:,seq).^1,input(:,seq).^2]';
                U=sigmoid(input(:,seq)',tempw(1),tempw(2),tempw(3),tempw(4)); %equation (12) with new model params
                numerror=0;
                for k=1:length(MOS)
                    if k<=max(nf,nb)+1
                        Y(k,1)=output(k,seq);
                        Z(k,1)=(Y(k,1)-v(2))/v(1);
                    else
                        Z(k,1)=b'*U(:,k-nb:k)'+Z(k-nf:k-1)'*f;
                        Y(k,1)=v(1)*Z(k,1)+v(2);

                    end
                    if Y(k,1)>(2*(CI_high(k,seq)-output(k,seq))+output(k,seq))||Y(k,1)<(-2*(output(k,seq)-CI_low(k,seq))+output(k,seq)) %outage (in equation (14))
                        numerror=numerror+1;
                    end
                end
                OR(seq)=numerror/length(MOS); %outage rate   
            end
            avgOR=mean(OR);
            performOR(nf,itr)=avgOR;
            if (itr-preitr)>=500
                    break;
            end
%             if dom_t(itr)>t_constraint(constid)
%                 break;
%             end

        end
%         if dom_t(itr)>t_constraint(constid)
%             break;
%         end
        nu=nu*1.2; %Algorithm 1, line 4
        preitr=itr;
        
% % %         for seq=1:m
% % %                 %X=[input(:,seq).^0,input(:,seq).^1,input(:,seq).^2]';
% % %                 U=sigmoid(input(:,seq)',tempw(1),tempw(2),tempw(3),tempw(4));
% % %                 numerror=0;
% % %                 errorlog=zeros(1,length(MOS));
% % %                 for k=1:length(MOS)
% % %                     if k<=max(nf,nb)+1
% % %                         Y(k,1)=output(k,seq);
% % %                         Z(k,1)=(Y(k,1)-v(2))/v(1);
% % %                     else
% % %                         Z(k,1)=b'*U(:,k-nb:k)'+Z(k-nf:k-1)'*f;
% % %                         Y(k,1)=v(1)*Z(k,1)+v(2);
% % % 
% % %                     end
% % %                     if Y(k,1)>(2*(CI_high(k,seq)-output(k,seq))+output(k,seq))||Y(k,1)<(-2*(output(k,seq)-CI_low(k,seq))+output(k,seq))
% % %                         numerror=numerror+1;
% % %                         errorlog(k)=1;
% % %                     end
% % %                 end
% % %                 OR(seq)=numerror/length(MOS);    
% % %         end
% % %         avgOR=mean(OR);

    end
    %% show the final results
    sigma2=0;
    for seq=1:m
        %X=[input(:,seq).^0,input(:,seq).^1,input(:,seq).^2]';
        U=sigmoid(input(:,seq)',w(1),w(2),w(3),w(4));
        error=0;
        errorlog=zeros(1,length(MOS));
        for k=1:length(MOS)
            if k<=max(nf,nb)+1
                Y(k,1)=output(k,seq);
                Z(k,1)=(Y(k,1)-v(2))/v(1);
            else
                Z(k,1)=b'*U(:,k-nb:k)'+Z(k-nf:k-1)'*f;
                Y(k,1)=v(1)*Z(k,1)+v(2);
                %Y(k,1)=sigmoid(Z(k,1),xw(1),xw(2),xw(3),xw(4));
                
            end
            if Y(k,1)>(2*(CI_high(k,seq)-output(k,seq))+output(k,seq))||Y(k,1)<(-2*(output(k,seq)-CI_low(k,seq))+output(k,seq))
                error=error+1;
                errorlog(k)=1;
            end
        end
        sigma2=sigma2+sum((Y-output(:,seq)).^2);
        OR(seq)=error/(length(MOS)-nf);
        LCC(seq)=corr(Y(nf+1:300,1),output(nf+1:300,seq));
        RRC(seq)=corr(Y(nf+1:300,1),output(nf+1:300,seq),'type','spearman');
        
%         figure(seq);
%         hold on;
%         ciplot(CI_low(nf+1:300,seq),CI_high(nf+1:300,seq),[135,206,250]/255);
%         plot(Y(nf+1:300,1),'b-','linewidth',2);
%         box on;
%         xlabel('$t/\mathrm{sec.}$','interpreter','latex','FontSize',30);
%         ylabel('$\mathsf{TVSQ}$','interpreter','latex','FontSize',24);
%         L=legend('Measured TVSQ (95% CI)','Predicted TVSQ');
    end
    mean(OR)
    sigma=sqrt(sigma2/(length(output)*m));
    DL(nf)=(log((length(MOS)-nf)*length(seqind))/(length(MOS)-nf)/length(seqind)*(nb+nf)+1)*avgOR;
%     performOR(nf,constid)=mean(OR);
%     dom_t(nf,constid)=max(-3./log(abs(roots(cat(1,1,-flipud(f))))));
end
%%
% figure(seq+1);
% stem(OR);
% figure(seq+2);
% plot(dom_t);
% figure(seq+3);
% plot(optlog);
% avgOR=mean(OR)
% %% Analyze the BIBO property 
% Xinf=sigmoid([-20:0.1:20],w(1),w(2),w(3),w(4));
% B=cat(1,0,flipud(b))';
% A=cat(1,1,-flipud(f))';
% % num=cat(2,0,flipud(b)');
% % den=cat(2,1,-flipud(f)');
% % sys=tf(num,den,1,'variable','z^-1');
% % [y]=impulse(sys,500);
% h = impz(B,A,50000);
% h1=sum(abs(h))
% outrange=h1*Xinf*v(1)+v(2);
% %outrange=(1-qfunc((outrange/100)*8-4))*100;
% figure(seq+4)
% %stem(h)
% plot([-20:0.1:20],outrange)
% %plot(Xinf,outrange)
% %% MDL
% DL(nf)=(log((length(MOS)-nf)*length(seqind))/(length(MOS)-nf)/length(seqind)*(nb+nf)+1)*mean(OR)
% %% static state
% Xinf=sigmoid([-20:0.1:20],0.1634,2.3172,-150,180);
% out=sum(b)/(1-sum(f))*Xinf;
% %subplot(2,1,1);
% %plot([-20:0.1:20],out);
% plot([-150:0.1:50],[-150:0.1:50]*v(1)+v(2));
% hold on
%  
% xw=[0.014,-1.10,5,180];
% out3=sigmoid([-150:0.1:50],xw(1),xw(2),xw(3),xw(4));
% plot([-150:0.1:50],out3);
% hold off
% % subplot(2,1,2);
% % plot(-50:0.01:50,sigmoid(-50:0.01:50,xw(1),xw(2),xw(3),xw(4)))
% 