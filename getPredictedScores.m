
testInp = iddata([],testData.InputData);
Y = sim(nlhw_b3_f4_k0,testInp);
Y = Y.OutputData;

predictedMOS =[];
%Y - each column has the rating
for i = 1:length(Y)
    yp = Y{1,i};
    predictedMOS =[predictedMOS ; yp(end)];
end

[corr(predictedMOS,gTruthTestMOS')  corr(predictedMOS,gTruthTestMOS','type','Spearman')]

[corr(avvasiTestMOS,gTruthTestMOS')  corr(avvasiTestMOS,gTruthTestMOS','type','Spearman')]