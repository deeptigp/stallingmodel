function plotInOut(inp,nonLinInp,nonLinOut,out,vidNum)
    figure;
    subplot(2,2,1)
    plot(inp)
    title(strcat('Input fed to the nonlinear module',num2str(vidNum)));
    
    subplot(2,2,2)
    plot(nonLinInp)
    title('Non-Linear Input');
    
    subplot(2,2,3)
    plot(nonLinOut)
    title('Non-Linear Output');
    
    subplot(2,2,4)
    plot(out)
    title('final Output');
end