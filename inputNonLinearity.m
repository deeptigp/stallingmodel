%given the raw input and its model parameters, we apply a non-linear
%function..
function ut = inputNonLinearity(in,w)
    ut = sigmoid(in',w(1),w(2),w(3),w(4));
end